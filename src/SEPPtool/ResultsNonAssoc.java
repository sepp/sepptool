package SEPPtool;

import javax.xml.bind.annotation.XmlRootElement;

import SEPPtool.cube.CubeTable;

/**
 * Stores results needed for prediction of non-association parameters (epsilon, m, sigma, mu)
 */
@XmlRootElement 
public class ResultsNonAssoc {

	InteractionData interactionData;
	CubeTable cubeTable;
	
	public ResultsNonAssoc()
	{
		
	}
	
	public ResultsNonAssoc(InteractionData interactionData, CubeTable cubeTable) {
		this.interactionData = interactionData;
		this.cubeTable = cubeTable;
	}
	
	public InteractionData getInteractionData() {
		return interactionData;
	}
	public void setInteractionData(InteractionData interactionData) {
		this.interactionData = interactionData;
	}
	public CubeTable getCubeTable() {
		return cubeTable;
	}
	public void setCubeTable(CubeTable cubeTable) {
		this.cubeTable = cubeTable;
	}
}
