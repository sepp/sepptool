package SEPPtool;

/**
 * Interface for all calculations that are meant to be performed in batch jobs.
 * Implementations are located in package SEPPtool.calculations
 * @author sebastian
 *
 */
public interface Calculation {

	/**
	 * 
	 * @param moldir
	 * @return true if this calculation has already been successfully performed for the given molecule
	 */
	public boolean calculationAlreadyPerformed(MoleculeDirectory moldir);
	
	/**
	 *  
	 * @param moldir
	 * @return true if all prerequisites to perform this calculation for the given molecule are met
	 */
	public boolean calculationReadyToPerform(MoleculeDirectory moldir);
	
	/**
	 * Perform the calculation for the given molecule
	 * @param moldir
	 * @return true on success
	 */
	public boolean perform(MoleculeDirectory moldir);
	
	/**
	 * 
	 * @return the String to show in the menu
	 */
	public String menuEntryString();
}
