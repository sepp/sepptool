package SEPPtool;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Data storage class for interaction data
 * @author sebastian
 *
 */
@XmlRootElement
public class InteractionData
{
	private double mu; // dipole moment in D
	private double Q; // quadrupole moment in DA
	private double alpha; // isotropic static polarizability in TODO
	private double C6; // dispersion coefficient in TODO
	
	public double getMu() {
		return mu;
	}
	public void setMu(double mu) {
		this.mu = mu;
	}
	public double getQ() {
		return Q;
	}
	public void setQ(double Q) {
		this.Q = Q;
	}
	public double getAlpha() {
		return alpha;
	}
	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}
	public double getC6() {
		return C6;
	}
	public void setC6(double c6) {
		C6 = c6;
	}
}