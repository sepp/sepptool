package SEPPtool.cosmo;

import java.util.Collections;
import java.util.HashMap;
import java.util.Vector;

import org.apache.commons.math3.analysis.ParametricUnivariateFunction;
import org.apache.commons.math3.fitting.SimpleCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoint;

/**
 * Calculate SAFT association parameters based on a COSMOFile object
 * @author sebastian
 *
 */
public class Association {
	
	COSMOFile cosmofile;
	
	// scaling variables that influence the calculation of SAFT-association-sites from "COSMO"-association-sites
	double c_hb, c_K, c_epsilonAB, negativeThreshold, positiveThreshold, offsetScaling;
	
	// a "site" is a Vector of SurfaceSegments
	// donorSites and acceptorSites are Vectors of "sites"
	Vector<Vector<SurfaceSegment>> donorSites=new Vector<Vector<SurfaceSegment>>();
	Vector<Vector<SurfaceSegment>> acceptorSites=new Vector<Vector<SurfaceSegment>>();
		
	/**
	 * @param cfile the COSMOFile with information about surface segments
	 * @param negativeThreshold sigma charge threshold for negative sigma charges (HB-donors)
	 * @param positiveThreshold sigma charge threshold for positive sigma charges (HB-acceptors)
	 * @param coherenceradius distance (in TODO:unit?) which defines whether 2 surface segments belong to the same coherent site 
	 * @param hotspotradius distance (in TODO:unit?) which defines hot spot (lowest/highest charge within this distance)
	 */
	public Association(COSMOFile cfile, double negativeThreshold, double positiveThreshold, double coherenceradius, double hotspotradius, double sigmaWeightingExponent)
	{
		this.cosmofile=cfile;
		this.negativeThreshold=negativeThreshold;
		this.positiveThreshold=positiveThreshold;
		createAssociationSites(negativeThreshold,positiveThreshold,coherenceradius,hotspotradius,sigmaWeightingExponent);
	}

	/**
	 * @param cfile the COSMOFile with information about surface segments
	 * @param negativeThreshold sigma charge threshold for negative sigma charges (HB-donors)
	 * @param positiveThreshold sigma charge threshold for positive sigma charges (HB-acceptors)
	 * @param coherenceradius distance (in TODO:unit?) which defines whether 2 surface segments belong to the same coherent site 
	 * @param hotspotradius distance (in TODO:unit?) which defines hot spot (lowest/highest charge within this distance)
	 * @param c_hb scaling for energy calculated from a pair of sigma charges
	 * @param c_K scaling factor for K^AB=kappa^AB*sigma^3
	 * @param c_eps scaling factor epsilon^AB
	 */
	public Association(COSMOFile cfile, double negativeThreshold, double positiveThreshold, double coherenceradius, double hotspotradius, double sigmaWeightingExponent, double offsetScaling, double c_hb, double c_K, double c_eps)
	{
		this(cfile,negativeThreshold,positiveThreshold,coherenceradius,hotspotradius,sigmaWeightingExponent);
		this.c_hb=c_hb;
		this.c_K=c_K;
		this.c_epsilonAB=c_eps;
		this.offsetScaling=offsetScaling;
	}
	
	/**
	 * algorithm:
	 * - find coherent patches that are
	 *   - above threshold sigma charge (below "negativeThreshold" or above "positiveThreshold"
	 *   - within a certain distance from each other ("coherenceradius")
	 * - within each coherent patch, find hot spots that have highest sigma-charge within a certain radius ("hotspotradius")
	 * - assign all segments within a coherent patch to the closest hot spot (weighting by sigma-charge of hotspot to the power of "sigmaWeightingExponent")
	 * 
	 * @param negativeThreshold the threshold for negative surface charge densities
	 * @param positiveThreshold the threshold for positive surface charge densities
	 * @param coherenceradius the distance to check for coherent segments
	 * @param hotspotradius
	 * @param sigmaWeightingExponent
	 */
	public void createAssociationSites(double negativeThreshold,double positiveThreshold, double coherenceradius, double hotspotradius, double sigmaWeightingExponent)
	{	
		// clear vectors (in case this function is called multiple times, e.g. re-calculation)
		donorSites.clear();
		acceptorSites.clear();
		
		// create a copy of all segments, because they are removed from the Vector later
		Vector<SurfaceSegment> allSegments = new Vector<SurfaceSegment>(cosmofile.getSegments());
		
		// sort allSegment with ascending averaged sigma charges
		Collections.sort(allSegments,new ComparatorAveragedSigma());
		
		int i_max=allSegments.size(); // separate variable, because size of allSegments changes
		for (int i=0;i<i_max;i++)
		{
			// get segment with most negative averaged sigma charges
			SurfaceSegment segment=allSegments.firstElement();
			
			// if the most negative averaged sigma charge is above threshold, get the segment with the most positive averaged sigma charge
			if (segment.getAveragedSigma()>negativeThreshold)
			{
				segment=allSegments.lastElement();
				
				// if the most positive averaged sigma charge is below the threshold, only segment that do not belong to any association site remain 
				if (segment.getAveragedSigma()<positiveThreshold) break;
			}
			
			// the coherent site belonging to SurfaceSegment "segment"
			Vector<SurfaceSegment> coherentSite=new Vector<SurfaceSegment>();
			
			// add "segment" to "coherentSite"
			coherentSite.add(segment);
			// remove "segment" from pool of unassigned segments
			allSegments.remove(segment);
			
			boolean addedNewSegment=true; // start with true to enter while-loops
			
			// loop while new segments are being added, because neighbors of newly added segments have to be checked for further coherent neighbors
			while (addedNewSegment)
			{
				addedNewSegment=false;
				// We cannot add segments to the Vector, we're iterating through.
				// We collect all segments to be added in "newSegments".
				Vector<SurfaceSegment> newSegments=new Vector<SurfaceSegment>();
				for (SurfaceSegment siteSegment:coherentSite)
				{
					// Iterate over all segment that are already part of the "coherentSite"
					// and then check all neighbor-segments within "coherenceradius" for their
					// sigma charge threshold
					Vector<SurfaceSegment> neighbors=cosmofile.getNeighborSurfaceSegments(siteSegment, coherenceradius, allSegments);
					for (SurfaceSegment neighbor:neighbors)
					{
						if ((siteSegment.getAveragedSigma()<0 && neighbor.getAveragedSigma()<negativeThreshold) ||
							(siteSegment.getAveragedSigma()>0 && neighbor.getAveragedSigma()>positiveThreshold) )
						{	// found coherent segment
							allSegments.remove(neighbor); // remove neighbor from the pool of unassigned segments
							newSegments.add(neighbor); // add neighbor to the list being added to coherentSite
							addedNewSegment=true; // remember, that segments have been added in order to stay in while-loop and check neighbors of newly added segments
						}
					}
				}
				// Done iterating over "coherentSite", now add all new segments.
				for (SurfaceSegment newSeg:newSegments) coherentSite.addElement(newSeg);
			}
			
			// "coherentSite" now contains a coherent patch. Split among hot spots
			
			// find hot spots
			HashMap<SurfaceSegment,Vector<SurfaceSegment>> hotSpotSites=new HashMap<SurfaceSegment,Vector<SurfaceSegment>>();
			for (SurfaceSegment siteSegment:coherentSite)
			{
				if (cosmofile.isHotSpot(siteSegment, hotspotradius, coherentSite))
				{
					//System.out.println("hotspot "+siteSegment.position[0]+","+siteSegment.position[1]+","+siteSegment.position[2]);
					hotSpotSites.put(siteSegment,new Vector<SurfaceSegment>());
				}
			}
			
			// assign all sites to nearest hot spot
			for (SurfaceSegment siteSegment:coherentSite) // iterate over all segments of coherent site
			{
				double smallestDistance=Double.MAX_VALUE;
				SurfaceSegment closestHotSpot=null;
				for (SurfaceSegment hotSpot:hotSpotSites.keySet()) // iterate over hot spots
				{
					double distance=hotSpot.distanceTo(siteSegment) / Math.pow(hotSpot.getAveragedSigma(), sigmaWeightingExponent);
					if (distance<smallestDistance)
					{
						smallestDistance=distance;
						closestHotSpot=hotSpot;
					}
				}
				// add siteSegment to the Site of the closest hot spot
				hotSpotSites.get(closestHotSpot).add(siteSegment);
			}
			
			for (Vector<SurfaceSegment> site:hotSpotSites.values())
			{
				if (site.get(0).getAveragedSigma()<0) donorSites.add(site);
				else acceptorSites.add(site);
			}
		}
	}
	
	public double[] calculateSAFTAssociationParameters(int iDonor, int iAcceptor)
	{
		return calculateSAFTAssociationParameters(
				getDonorSites().get(iDonor), // A
				getAcceptorSites().get(iAcceptor), // B
				c_hb, c_K, c_epsilonAB,
				negativeThreshold*offsetScaling, // A is donor (positive charge -> negative sigma charge)
				positiveThreshold*offsetScaling); // B is acceptor (negative charge -> positive sigma charge)
	}
	
	
	/**
	 * 
	 * @param siteA
	 * @param siteB
	 * @param c_hb
	 * @param c_K
	 * @param c_epsilonAB
	 * @param offsetA
	 * @param offsetB
	 * @return array with 2 entries: first is kappa*sigma^3, second is epsilonAB/k
	 */
	public static double[] calculateSAFTAssociationParameters(Vector<SurfaceSegment> siteA, Vector<SurfaceSegment> siteB,
			final double c_hb, final double c_K, final double c_epsilonAB, final double offsetA, final double offsetB)
	{
		double K_epsilon[]={10,1500}; // start values
		
		if (c_hb<=0 || c_K<=0 || c_epsilonAB<=0) return new double[] { Double.NaN, Double.NaN };
		
		Vector<WeightedObservedPoint> DeltaCOSMO=new Vector<WeightedObservedPoint>();
		for (int T=250;T<=600;T+=10)
		{
			double deltaCOSMO=calculateDeltaCOSMO(siteA, siteB, T, c_hb, offsetA, offsetB);
			// the weight of deltaCOSMO^(-2) leads to a minimization of relative deviations [(y-y')/y']^2
			DeltaCOSMO.add(new WeightedObservedPoint(Math.pow(deltaCOSMO, -2), T, deltaCOSMO ));
		}
		
		// calculate starting values from Delta with smallest and highest temperature (2 equations, 2 unknowns)
		
		// neglect "-1" for calculating epsilonAB/k:
		// epsilonAiBj/k = ln(Delta1/Delta2) / (1/T1 - 1/T2)
		K_epsilon[1]=Math.log(DeltaCOSMO.firstElement().getY()/DeltaCOSMO.lastElement().getY()) / 
				(1/DeltaCOSMO.firstElement().getX() - 1/DeltaCOSMO.lastElement().getX());
		
		// K = Delta1 / [ exp(epsilon/kT) - 1 ]
		K_epsilon[0]=DeltaCOSMO.firstElement().getY() / (Math.exp(K_epsilon[1]/DeltaCOSMO.firstElement().getX())-1);
		
		// those values include the scaling factors: divide by scaling factors
		K_epsilon[0] /= c_K;
		K_epsilon[1] /= c_epsilonAB;
		
		//System.out.printf("\nStarting values: K=%10.5g eps=%10.5g\n", K_epsilon[0],K_epsilon[1]);
		
		ParametricUnivariateFunction deltaSAFT=new ParametricUnivariateFunction(){

			@Override
			public double[] gradient(double T, double... param) {
				double K=param[0];
				double epsilonAB_k=param[1];
				if (K<0||epsilonAB_k<0) return new double[] {Double.NaN, Double.NaN};
				
				double ret[]=new double[2];
				
				// d(Delta)/d(K)
				ret[0] = c_K*(Math.exp(c_epsilonAB*epsilonAB_k/T)-1);
				
				// d(Delta)/d(epsilonAB/k)
				ret[1] = c_K*K*c_epsilonAB/T*Math.exp(c_epsilonAB*epsilonAB_k/T);
				
				return ret;
			}

			@Override
			public double value(double T, double... param) {
				double K=param[0];
				double epsilonAB_k=param[1];
				if (K<0||epsilonAB_k<0) return Double.NaN;
				return calculateDeltaSAFT(T,K,epsilonAB_k, c_K, c_epsilonAB);
			}
			
		};
		
		SimpleCurveFitter fitter=SimpleCurveFitter.create(deltaSAFT,K_epsilon);
		K_epsilon=fitter.fit(DeltaCOSMO);
		//System.out.printf("Fitted values: K=%10.5g eps=%10.5g\n\n", K_epsilon[0],K_epsilon[1]);
		
		return K_epsilon;
	}
	
	public Vector<Vector<SurfaceSegment>> getDonorSites() {
		return donorSites;
	}

	public Vector<Vector<SurfaceSegment>> getAcceptorSites() {
		return acceptorSites;
	}

	public static double calculateDeltaCOSMO(Vector<SurfaceSegment> siteA, Vector<SurfaceSegment> siteB, double T, double c_hb, double offsetA, double offsetB)
	{
		double delta=0.0;
		for(SurfaceSegment segmentA:siteA)
		{
			for(SurfaceSegment segmentB:siteB)
			{
				//System.err.printf("sigA=%10g offA=%10g sigB=%10g offB=%10g\n", segmentA.getAveragedSigma(), offsetA, segmentB.getAveragedSigma(), offsetB);
				double e = (segmentA.getAveragedSigma()-offsetA) * (segmentB.getAveragedSigma()-offsetB);
				double f = Math.exp( -c_hb * e / 8.134 / T )  -  1;
				delta += segmentA.getArea() * segmentB.getArea() * f;
			}
		}
		return delta;
	}
	
	public static double calculateDeltaSAFT(double T, double K, double epsilonAB_k, double c_K, double c_epsilonAB)
	{
		return c_K * K * ( Math.exp(c_epsilonAB*epsilonAB_k/T) - 1 );
	}
	
	
	
	/////////////////////////////////////
	// getter and setter for variables //
	/////////////////////////////////////
	
	public double getC_hb() {
		return c_hb;
	}

	public void setC_hb(double c_hb) {
		this.c_hb = c_hb;
	}

	public double getC_K() {
		return c_K;
	}

	public void setC_K(double c_K) {
		this.c_K = c_K;
	}

	public double getC_epsilonAB() {
		return c_epsilonAB;
	}

	public void setC_epsilonAB(double c_epsilonAB) {
		this.c_epsilonAB = c_epsilonAB;
	}

	
	
	////////////////////////////////////////
	// convenience functions for printing //
	////////////////////////////////////////
	
	public void printDeltas(double c_hb)
	{
		for (Vector<SurfaceSegment> siteA:donorSites)
		{
			for (Vector<SurfaceSegment> siteB:acceptorSites)
			{
				for (double T=10;T<1000;T+=10)
				{
					System.out.printf("%10.5g %20.15g\n",T,calculateDeltaCOSMO(siteA,siteB,T,c_hb,0,0)); // no offset!!!
				}
				System.out.println();
			}
		}
	}
	
	public void printAssociationSites()
	{
		printAssociationSites(donorSites);
		printAssociationSites(acceptorSites);
	}
	
	public void printAssociationSites(Vector<Vector<SurfaceSegment>> sites)
	{
		for (Vector<SurfaceSegment> site:sites)
		{
			System.out.println("===Site===");
			double sitearea=0.0;
			for (SurfaceSegment s:site)
			{
				System.out.printf("%4d %2d %10.5g %10.5g %10.5g %10.5g %10.5g %10.5g %10.5g %10.5g\n" ,
						s.getN(), s.getAtom(),
						s.getPosition()[0], s.getPosition()[1], s.getPosition()[2],
						s.getCharge(), s.getArea(), s.getCharge()/s.getArea(), s.getAveragedSigma(),s.getPotential());
				sitearea += s.getArea();
			}
			System.out.printf("#segments=%d\n", site.size());
			System.out.printf("area=%g\n", sitearea);
		}
	}
}
