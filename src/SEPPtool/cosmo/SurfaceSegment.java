package SEPPtool.cosmo;

/**
 * Representation of a COSMO surface segment
 * @author sebastian
 *
 */
public class SurfaceSegment {
	int n;
	int atom; // the atom to which this surface segment belongs
	double position[]=new double[3]; // x,y,z coordinates in a.u.
	double charge; // e?
	double area; // Angstrom^2
	double potential;
	// cf. Klamt1998 J.Phys.Chem.A 102 5074-5085 eq. 11
	double averagedSigma; // e/Angstrom^2
	
	public int getN() {
		return n;
	}
	public void setN(int n) {
		this.n = n;
	}
	public int getAtom() {
		return atom;
	}
	public void setAtom(int atom) {
		this.atom = atom;
	}
	public double[] getPosition() {
		return position;
	}
	public void setPosition(double[] position) {
		this.position = position;
	}
	public double getCharge() {
		return charge;
	}
	public void setCharge(double charge) {
		this.charge = charge;
	}
	public double getArea() {
		return area;
	}
	public void setArea(double area) {
		this.area = area;
	}
	public double getPotential() {
		return potential;
	}
	public void setPotential(double potential) {
		this.potential = potential;
	}
	
	public double getSigma()
	{
		return charge/area;
	}
	
	
	public double getAveragedSigma() {
		return averagedSigma;
	}
	public void setAveragedSigma(double averagedSigma) {
		this.averagedSigma = averagedSigma;
	}
	
	/**
	 * 
	 * @param seg2
	 * @return the distance from this segment to the parameter seg2 in a.u.
	 */
	public double distanceTo(SurfaceSegment seg2)
	{
		double distance_squared=0.0;
		double position2[]=seg2.getPosition();
		for (int i=0;i<3;i++)
			distance_squared += Math.pow(position[i]-position2[i], 2);
		return Math.sqrt(distance_squared);
	}
	
	
}
