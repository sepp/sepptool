package SEPPtool.cosmo;

import java.util.Comparator;

/**
 * Comparator for charge densities
 * @author sebastian
 *
 */
public class ComparatorSigma implements Comparator<SurfaceSegment>
{
	@Override
	public int compare(SurfaceSegment o1, SurfaceSegment o2) {
		if (o1.getSigma()<o2.getSigma()) return -1;
		else if (o1.getSigma()>o2.getSigma()) return 1;
		else return 0;
	}	
}