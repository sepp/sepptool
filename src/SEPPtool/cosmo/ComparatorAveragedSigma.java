package SEPPtool.cosmo;

import java.util.Comparator;

/**
 * Comparator for averaged charge densities
 * @author sebastian
 *
 */
public class ComparatorAveragedSigma implements Comparator<SurfaceSegment>
{
	@Override
	public int compare(SurfaceSegment o1, SurfaceSegment o2) {
		if (o1.getAveragedSigma()<o2.getAveragedSigma()) return -1;
		else if (o1.getAveragedSigma()>o2.getAveragedSigma()) return 1;
		else return 0;
	}
}