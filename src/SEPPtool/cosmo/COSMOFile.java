package SEPPtool.cosmo;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.Vector;

import SEPPtool.cube.Point;

/**
 * Data storage and parsing of COSMO-files 
 * @author sebastian
 *
 */
public class COSMOFile {
	
	int nps=0;
	double area,volume; // unit?
	Vector<SurfaceSegment> segments = new Vector<SurfaceSegment>();
	Vector<Atom> atoms = new Vector<Atom>();

	public COSMOFile(String file) throws IOException
	{
		readCOSMOFile(file);
		averageSegments(1.0 /*AA*/); // Klamt1998: 0.5 AA
	}
	
	public void readCOSMOFile(String file) throws IOException
	{
		// Open the file
		
		FileInputStream fstream = new FileInputStream(file);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

		String strLine;
		String strSection="";

		//Read File Line By Line
		while ((strLine = br.readLine()) != null)   {
			
			if (strLine.startsWith("$")) // this is the marking for a new section
			{
				strSection=strLine.trim().substring(1);
				continue; // do not process this line any further
			}
			
			strLine=strLine.trim(); // discard white spaces at beginning and end of line
			
			if (strLine.startsWith("#")) continue; // this is a comment in the COSMO file

			// handle strLine differently depending on which section we are in
			switch(strSection)
			{
			case "cosmo_data":
				String[] datum=strLine.split("=");
				if (datum.length!=2) break;
				
				switch(datum[0].trim())
				{
				case "nps":
					nps=Integer.parseInt(datum[1].trim());
					segments.ensureCapacity(nps); // avoid resizing Vector after each addition of a SurfaceSegment
					break;
				case "area":   area   = Double.parseDouble(datum[1]); break;
				case "volume": volume = Double.parseDouble(datum[1]); break;
				}
				break;
			case "coord_rad":
				Scanner scanner=new Scanner(strLine);
				Atom atom = new Atom();
				atom.setIndex(scanner.nextInt());
				for (int i = 0; i < 3; i++) atom.set(i, scanner.nextDouble()*0.529); // from a.u. to Angstrom
				atom.setElement(scanner.next());
				atom.setRadius(scanner.nextDouble());
				atoms.add(atom);
				break;
			case "segment_information":
				scanner = new Scanner(strLine);
				SurfaceSegment seg = new SurfaceSegment();
				seg.setN(scanner.nextInt());
				seg.setAtom(scanner.nextInt());
				double pos[] = new double[3];
				for (int i=0;i<3;i++) pos[i]=scanner.nextDouble()*0.529; // from a.u. to Angstrom
				seg.setPosition(pos);
				seg.setCharge(scanner.nextDouble());
				seg.setArea(scanner.nextDouble()); // is Angstrom^2 already
				scanner.nextDouble(); // don't save Charge/Area, information is redundant
				seg.setPotential(scanner.nextDouble());
				segments.add(seg);
				break;
			}

		}

		//Close the input stream
		br.close();
		
		//printSegments();
	}
	
	public Vector<SurfaceSegment> getSegments() {
		return segments;
	}
	
	public void setSegments(Vector<SurfaceSegment> segments) {
		this.segments = segments;
	}

	public Vector<Atom> getAtoms() {
		return atoms;
	}
	
	/**
	 * Calculates the i-th sigma moment according to Klamt 1998.
	 * J. Phys. Chem A 102, pp 5074-5085 eq. 29 discretized with Area=d sigma * p(sigma)
	 * @param i the order of the sigma moment
	 * @return the value of the sigma moment
	 */
	public double getSigmaMoment(int i)
	{
		double m=0;
		for (SurfaceSegment s:segments)
		{
			m += s.getArea() * Math.pow(s.getAveragedSigma(), i);
		}
		return m;
	}
	
	public boolean isHotSpot(SurfaceSegment segment, double radius, Vector<SurfaceSegment> hotspotsegments)
	{
		boolean positiveHotSpot=true, negativeHotSpot=true;
		double sigma=segment.getAveragedSigma();
		//int n=0;
		
		for (SurfaceSegment seg2:hotspotsegments)
		{
			if (segment==seg2) continue; // don't compare to itself
			
			// is second segment within given radius?
			if (segment.distanceTo(seg2) < radius)
			{
				//n++;
				double sigma2=seg2.getAveragedSigma();
						
				if (sigma2>sigma) positiveHotSpot=false;
				else if (sigma2<sigma) negativeHotSpot=false;
				// if both are equal it will be marked as hot spot
			}
		}
		
		//System.out.print("n="+n+" hostpot="+(positiveHotSpot||negativeHotSpot));
		
		return ((sigma>0)&&positiveHotSpot) || ((sigma<0)&&negativeHotSpot);
	}
	
	public boolean isHotSpot(SurfaceSegment segment, double radius)
	{
		return isHotSpot(segment,radius,segments);
	}

	public void printSegments()
	{
		for(SurfaceSegment s:segments)
		{
			System.out.printf("%4d %2d %10.5g %10.5g %10.5g %10.5g %10.5g %10.5g %10.5g %10.5g %b\n" ,
					s.getN(), s.getAtom(),
					s.getPosition()[0], s.getPosition()[1], s.getPosition()[2],
					s.getCharge(), s.getArea(), s.getCharge()/s.getArea(),s.getAveragedSigma(),s.getPotential(),isHotSpot(s,2));
		}
	}
	
	
	
	public Vector<SurfaceSegment> getNeighborSurfaceSegments(SurfaceSegment seg, double radius)
	{
		return getNeighborSurfaceSegments(seg, radius, getSegments());
	}
	
	public Vector<SurfaceSegment> getNeighborSurfaceSegments(SurfaceSegment seg, double radius, Vector<SurfaceSegment> segments)
	{
		Vector<SurfaceSegment> neighbors=new Vector<SurfaceSegment>();
		
		for (SurfaceSegment seg2:segments)
		{
			if (seg!=seg2 && seg.distanceTo(seg2)<radius) neighbors.add(seg2);
		}
		
		return neighbors;
	}
	
	// J. Phys. Chem A 102, pp 5074-5085 eq. 11
	public void averageSegments(double r_av)
	{
		double r_av2 = r_av*r_av;
		
		for (SurfaceSegment seg_nu:segments)
		{
			double numerator=0.0;
			double denominator=0.0;
			for (SurfaceSegment seg_mu:segments)
			{
				double d_munu=seg_mu.distanceTo(seg_nu);
				if (d_munu<r_av) // only do averaging within a distance of r_av
				{
					double sigma_mu=seg_mu.getSigma();
					double r_mu2=seg_mu.getArea()/Math.PI;
					double term = r_mu2*r_av2 / (r_mu2+r_av2) * Math.exp( -d_munu*d_munu / (r_mu2+r_av2) );
					numerator += sigma_mu * term;
					denominator += term;
				}
			}
			seg_nu.setAveragedSigma(numerator/denominator);
		}
	}
	
	public int getNPS()
	{
		return nps;
	}


	public class Atom extends Point
	{
		int index;
		String element;
		double radius;
	
		public int getIndex() {
			return index;
		}
		public void setIndex(int index) {
			this.index = index;
		}
		public String getElement() {
			return element;
		}
		public void setElement(String element) {
			this.element = element;
		}
		public double getRadius() {
			return radius;
		}
		public void setRadius(double radius) {
			this.radius = radius;
		}
	}
}