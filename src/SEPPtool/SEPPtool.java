package SEPPtool;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Vector;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import SEPPtool.calculations.Interactions;
import SEPPtool.calculations.Cubegen;
import SEPPtool.calculations.Dynpol;
import SEPPtool.calculations.Formchk;
import SEPPtool.calculations.GenerateCubeTable;
import SEPPtool.calculations.GeometryOptimization;
import SEPPtool.calculations.Polar;
import SEPPtool.cosmo.Association;
import SEPPtool.cosmo.COSMOFile;
import SEPPtool.cosmo.SurfaceSegment;
import SEPPtool.cube.CubeTable;
import SEPPtool.cube.CubeTable.CubeTableEntry;


/**
 * SEPPtool performs quantum mechanical calculations needed to predict
 * PCP-SAFT parameters. The main program lets the user choose which
 * calculations to perform either directly or send to a batch queue
 * of a computer cluster, e.g. slurm at the RWTH ITC HPC
 * 
 * If run without parameters, starts in interactive mode.
 * A molecule name can be given as first parameter, followed by a list
 * of numbers (1-7) for the calculations which should be performed.
 * If the molecule name is the only parameter, all calculations will be
 * performed.
 * 
 * @author sebastian
 *
 */
public class SEPPtool {
	
	final static String pathConfigFile="SEPPtool.config";
	public static Config config;
	final static String molecule_subdirectory = "molecules";
	public static final boolean LEGACY = true; // set to true to work with older code at the Institute for Technical Thermodyamics, RWTH Aachen University
	static Scanner scanner = new Scanner(System.in);
	
	static Calculation[] calculations = new Calculation[] {
			new GeometryOptimization(),
			new Polar(),
			new Formchk(),
			new Cubegen(),
			new GenerateCubeTable(),
			new Dynpol(),
			new Interactions(),
			};
	
	// which calculations to run within the same batch job
	static int[][] calculationCluster = new int[][] {
			 { 1 }, 
			 { 2 },
			 { 3, 4, 5 },
			 { 6, 7 }
			};

	/**
	 * If no arguments are given, SEPPtool starts in interactive mode, otherwise in batch mode.
	 * First argument is the molecule name. If only one parameter is given, all calculations
	 * are sequentially being performed. With any futher parameter, the number of calculation
	 * can be given, currently 1-7. 
	 * @param args
	 */
	public static void main(String[] args) {
		
		// read the config file, or create one if none is found
		readConfigFile();
		
		if (args.length > 0) // batch mode
		{
			String molecule_name = args[0];
			MoleculeDirectory moldir = new MoleculeDirectory(molecule_name);
			if (!moldir.getFile().isDirectory())
			{
				System.out.println("Could not read molecule directory "+moldir.getFile().getAbsolutePath());
				System.exit(1);
			}
			
			if (args.length==1) // only molecule name is given -> perform all calculations
			{
				// Perform all calculations. Exit if a calculation fails.
				for (int i = 0; i < calculations.length; i++) if (!perform(calculations[i], moldir)) System.exit(2);
			}
			else
			{
				for (int i_arg = 1; i_arg < args.length; i_arg++)
				{
					// command line parameter starts with 1, array starts at index 0
					int i_calc = Integer.parseInt(args[i_arg]) - 1;
					// perform calculation and exit if calculation fails
					if (!perform(calculations[i_calc], moldir)) System.exit(2);
				}
			}
			
			return;
		}
		
		// interactive mode
		while (true)
		{
			System.out.println("");
			System.out.println("0 exit");
			System.out.println("1 list all molecules");
			System.out.println("2 view a molecule");
			System.out.println("3 create a new molecule");
			System.out.println("4 calculate PC-SAFT parameters");
			
			int inputInt = scanner.nextInt();
			 
			switch(inputInt)
			{
			case 0:
				System.exit(0);
				break;
			case 1:
				listMolecules();
				break;
			case 2:
				System.out.print("Enter molecule mame: ");
				String molecule_name = scanner.next().trim();
				MoleculeDirectory moldir = new MoleculeDirectory(molecule_name);
				if (moldir.getFile().exists())
				{
					if (moldir.getFile().isDirectory())
						moleculeMenu(moldir);
					else
						System.out.println("File " + moldir.getFile().getAbsolutePath() + " exists, but is not a directory!");
				}
				else
				{
					System.out.println("Molecule " + molecule_name + "does not exist");
				}
				break;
			case 3:
				System.out.print("Enter molecule mame: ");
				molecule_name = scanner.next().trim();
				moldir = new MoleculeDirectory(molecule_name);
				if (!moldir.getFile().exists())
				{
					if (!moldir.getFile().mkdirs())
					{
						System.out.println("Could not create directory for molecule \""+molecule_name+"\"");
						break;
					}
				}
				else
				{
					System.out.println("Directory for molecule \""+molecule_name+"\" already exists");
				}
				moleculeMenu(moldir);
				break;
			case 4:
				scanner.nextLine(); // advance till next line (anything between user pressing "4" and user pressing "enter" to come to this point
				System.out.println("enter names of molecules (separated by space):");
				String line = scanner.nextLine();
				String[] molecules = line.split(" ");
				Vector<MoleculeDirectory> moldirs = new Vector<MoleculeDirectory>();
				for (String molecule : molecules)
				{
					if ("".equals(molecule)) continue; // in case user separated molecules with multiple space characters
					
					moldir = new MoleculeDirectory(molecule);
					
					// check if at least directory exists
					if (!moldir.getFile().isDirectory())
					{
						System.out.println("Molecule " + molecule + " does not exist, skipping it.");
						continue;
					}
					
					moldirs.add(moldir);
				}
				
				Vector<Double> v_epsilon = new Vector<Double>();
				Vector<Double> v_alpha_IP = new Vector<Double>();
				Vector<Double> v_m = new Vector<Double>();
				Vector<Double> v_sigma = new Vector<Double>();
				Vector<MoleculeDirectory> toRemove = new Vector<MoleculeDirectory>();
				int longest_name=0;
				for (MoleculeDirectory l_moldir : moldirs)
				{
					try
					{
						JAXBContext jaxbContext = JAXBContext.newInstance(CubeTable.class);
					    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
					    CubeTable cubeTable = (CubeTable) jaxbUnmarshaller.unmarshal(l_moldir.file_cubetable);
					    
					    jaxbContext = JAXBContext.newInstance(InteractionData.class);
					    jaxbUnmarshaller = jaxbContext.createUnmarshaller();
					    InteractionData interactionData = (InteractionData) jaxbUnmarshaller.unmarshal(l_moldir.file_interactiondata);
					    
					    CubeTableEntry entry = cubeTable.getCubeTableEntryAtEnclosedElectronPercentage(0.957613);
					    if (entry==null) throw new Exception("Error getting data from cube table");
					    
					    // from here on, data are successfully read
					    if (l_moldir.getName().length() > longest_name) longest_name = l_moldir.getName().length();
					    
					    
					    
					    final double au_to_AA=0.52917721067; 
						final double kB=3.1668114e-6; // Eh/K
						double C6QM_k = interactionData.getC6() * Math.pow(au_to_AA,6)/kB;
						
					    double mu = interactionData.getMu();
					    double V = -2.5776 + entry.getVolume() + 1.9911 * mu;
					    double alpha_geo = -0.31962 + 1.4693 * entry.getNonsphericity();
					    double m = 2.0 * alpha_geo - 1.0;
					    double sigma = Math.pow(V / m * 6.0 / Math.PI, 1.0 / 3.0);
					    double C6_k = C6QM_k + 56382 * mu * mu * interactionData.getAlpha();
					    double epsilon_k = -83.398 + 2.0696 * C6_k / 4 / m / m / Math.pow(sigma, 6) *
					    		Math.pow(entry.getSurfaceArea() / m / Math.PI / sigma / sigma , 2);
					    
					    v_epsilon.add(epsilon_k);
					    v_alpha_IP.add(interactionData.getAlpha());
					    v_m.add(m);
					    v_sigma.add(sigma);
					    System.out.println(l_moldir.getName());
					    System.out.println("sigma = " + sigma + " AA");
					    System.out.println("m = " + m);
					    System.out.println("epsilon_k = " + epsilon_k + " K");
					    System.out.println("mu = " + mu + " D");
					}
					catch (Exception e) 
					{
						System.err.print("Could not find result file for "+l_moldir.getName()+": ");
						System.err.println(e.getLocalizedMessage());
						toRemove.add(l_moldir);
					}
				}
				
				// remove molecules with missing results file
				for (MoleculeDirectory removeMolDir:toRemove)
				{
					moldirs.remove(removeMolDir);
				}
				
				// now indices of moldir and v_[parametername] match again
				for (int i = 0; i < moldirs.size(); i++)
				for (int j = i + 1; j < moldirs.size(); j++)
				{
					double sig_i = v_sigma.get(i);
					double sig_j = v_sigma.get(j);
					
					double eps_i = v_epsilon.get(i);
					double eps_j = v_epsilon.get(j);
					
					//epsilon * sigma^6
					double eps_sig6_i = eps_i * Math.pow(sig_i, 6);
					double eps_sig6_j = eps_j * Math.pow(sig_j, 6);
					
					// (sigma_ij)^6
					double sig6_ij = Math.pow( ( sig_i + sig_j ) * 0.5, 6);
					
					// segment-based alphas
					double alpha_i = v_alpha_IP.get(i) / v_m.get(i);
					double alpha_j = v_alpha_IP.get(j) / v_m.get(j);
					
					// london's combining rule
					double eps_ij = 2.0 / sig6_ij * ( eps_sig6_i * alpha_i  * eps_sig6_j * alpha_j ) / 
							( eps_sig6_i * Math.pow(alpha_j, 2) +  eps_sig6_j * Math.pow(alpha_i, 2) );
					
					// calculate and print k_ij
					double k_ij = 1 - eps_ij / Math.sqrt(eps_i * eps_j);
					
					System.out.printf("k_ij %"+longest_name+"s %"+longest_name+"s = %10g\n",
							moldirs.get(i).getName(),
							moldirs.get(j).getName(),
							k_ij);
				}
				
				// ASSOCIATION
				final double r_avg = 1.0;
				final double sigma_neg=-0.0102;
				final double sigma_pos=0.0096;
				final double r_coherence=0.7;
				final double r_hotspot=0.85;
				final double weightingExponent=1.0;
				final double offsetScaling=0.0;
				
				final double c_hb=8.5442382508944E7;
				final double c_K=37.4421922605258;
				final double c_epsilon=1.0;
				
				// map name of the site to a SEPP association site intermedia model
				HashMap<String,Vector<SurfaceSegment>> donors=new HashMap<String,Vector<SurfaceSegment>>();
				HashMap<String,Vector<SurfaceSegment>> acceptors=new HashMap<String,Vector<SurfaceSegment>>();
				
				for (int i = 0; i < moldirs.size(); i++)
				{
					try {
						
						// read cosmo file
						COSMOFile cosmo_file=new COSMOFile(moldirs.get(i).getFile().getAbsolutePath()+"/"+moldirs.get(i).getName()+"_c0.cosmo");
						// perform averaging of surface charges
						cosmo_file.averageSegments(r_avg);
						// create an Association object which handles calculation of SAFT parameters
						Association association=new Association(cosmo_file,sigma_neg,sigma_pos,r_coherence,r_hotspot,weightingExponent,offsetScaling,c_hb,c_K,c_epsilon);

						//// create the right number of donors and acceptors				
						int i_site=1;
						for (Vector<SurfaceSegment> donor:association.getDonorSites())
						{
							// link pSAFTbuilder-donor to SAFTsuite-donor
							donors.put(moldirs.get(i).getName() + " Donor "+i_site, donor);
							i_site++;
						}
						System.out.printf("%"+longest_name+"s %2d Donors", moldirs.get(i).getName(), i_site-1);
						
						i_site=1;
						for (Vector<SurfaceSegment> acceptor:association.getAcceptorSites())
						{
							// link pSAFTbuilder-acceptor to SAFTsuite-acceptor
							acceptors.put(moldirs.get(i).getName() + " Acceptor "+i_site, acceptor);
							i_site++;
						}
						System.out.printf(" and %2d acceptors\n", i_site-1);
					} catch (IOException e) {
						System.err.println("No COSMO-file found for "+moldirs.get(i).getName()+".");
					}
				}
				
				// parameters for all donor-acceptor pairs
				for (String donor:donors.keySet())
				for (String acceptor:acceptors.keySet())
				{
					// calculate K^AB and epsilon^AB
					double[] K_epsilon=Association.calculateSAFTAssociationParameters(donors.get(donor),acceptors.get(acceptor),
							c_hb, c_K, c_epsilon, sigma_neg*offsetScaling, sigma_pos*offsetScaling);
					double K_AB       = K_epsilon[0];
					double epsilon_AB = K_epsilon[1];
					
					System.out.println(donor);
					System.out.println(acceptor);
					System.out.println("K^AB = " + K_AB);
					System.out.println("epsilon^AB = " + epsilon_AB);
				}
			}
		}
	}
	
	public static void moleculeMenu(MoleculeDirectory moldir)
	{
		while (true)
		{
			System.out.println("");
			System.out.println(moldir.getName());
			System.out.println("");
			moldir.printFileInfo();
			System.out.println("");
			String format = "%3d %6s %s\n";
			System.out.printf(format, 0, "......","return to main menu");
			System.out.printf(format, 1, (moldir.file_gauss_geo_com.isFile() || moldir.file_cosmo.isFile()) ? "[done]" : "......","enter starting geometry");
			System.out.printf(format, 2, "......","perform calculations with slurm batch system");
			System.out.println();
			for (int i=0;i<calculations.length;i++)
			{
				System.out.printf(format, i+3,
						calculations[i].calculationAlreadyPerformed(moldir) ? "[done]" : calculations[i].calculationReadyToPerform(moldir) ? "......" : "",
						calculations[i].menuEntryString());
			}
			
			int inputInt = SEPPtool.scanner.nextInt();
			
			if (inputInt == 0) return;
			else if (inputInt == 1) geometryMenu(moldir);
			else if (inputInt == 2) slurmMenu(moldir);
			else if (inputInt > 1 && inputInt < 3 + calculations.length)
			{
				perform(calculations[inputInt-3], moldir);
			}
			else System.out.println("invalid input");
		}
	}
	
	public static void geometryMenu(MoleculeDirectory moldir)
	{
		while (true)
		{
			System.out.println("geometry menu: " + moldir.getName());
			System.out.printf("%-50s\n","0  return to main menu");
			System.out.printf("%-50s\n","1  enter SMILES (not recommended for large molecules, check generated conformation!)");
			System.out.printf("%-50s\n","2  choose geometry file (xyz,sdf,cosmo,com)");
			//System.out.printf("%-50s\n","3  manually enter xyz coordinates");
			//System.out.printf("%-50s\n","4  manually enter sdf coordinates");
			
			int inputInt = scanner.nextInt();
			
			switch(inputInt)
			{
			case 0:
				return;  // leave geometry menu
			case 1: // SMILES
				System.out.println("enter SMILES");
				String smiles = scanner.next();
				
				ArrayList<String> alist = new ArrayList<String>();

				// add the list of commands to the list
				alist.add(config.getPathObabel());
				alist.add("--title");
				alist.add(moldir.getName() +" geometry optimizazion");
				alist.add("-xb");
				alist.add("--gen3D");
				alist.add("--minimize");
				alist.add("--ff");
				alist.add("MMFF94");
				alist.add("-xk");
				alist.add("%mem=30GB\n# opt b3lyp geom=connectivity tzvp");
				alist.add("-:" + smiles + "");
				alist.add("-o");
				alist.add("com");
				alist.add("-O");
				alist.add(moldir.file_gauss_geo_com.getAbsolutePath());
				
				ProcessRunner proc = new ProcessRunner(alist);
				try {
					proc.run();
				} catch (IOException|InterruptedException e) {
					log(moldir, "Error calling obabel ("+alist.get(0)+")");
					break;
				}	

				if (proc.getExitValue() == 0 && proc.getErr().get(0).contains("1 molecule converted"))
				{
					log(moldir, "Input file for geometry optimization with gaussian generated from SMILES \""+smiles+"\"");
					return; // leave geometry menu
				}
				else
				{
					log(moldir, "Error converting SMILES \"" + smiles + "\" to gaussian input file:");
					for (String s:proc.getErr()) log(moldir, s);
				}
				
				break;
			case 2: // choose file
				System.out.println("not implemented yet");
				break;
			case 3:
				System.out.println("not implemented yet");
				break;
			case 4:
				System.out.println("not implemented yet");
				break;
			default:
				System.out.println("invalid input");
			}
		}
	}
	
	public static void slurmMenu(MoleculeDirectory moldir)
	{
		while (true)
		{
			System.out.println("slurm menu: " + moldir.getName());
			System.out.printf("%-50s\n","0  return to main menu");
			System.out.printf("%-50s\n","1  create slurm scripts");
			System.out.printf("%-50s\n","2  create and submit slurm scripts");
			
			int inputInt = scanner.nextInt();
			
			switch(inputInt)
			{
			case 0:
				return;
			case 1:
				createSlurmScripts(moldir);
				break;
			case 2:
				if (createSlurmScripts(moldir))
				{
					if (submitSlurmScripts(moldir)) return; //on success return to main menu
					else break; // on failure stay in slurm menu
				}
			}
		}
	}
	
	public static boolean createSlurmScripts(MoleculeDirectory moldir)
	{
		File slurmDir = new File(moldir.getFile().getAbsolutePath() + "/slurm");
		if (!slurmDir.isFile()) slurmDir.mkdir();
		
		try
		{
			String jarFile = SEPPtool.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			
			for (int i = 0; i < calculationCluster.length; i++)
			{
				File file = new File(moldir.getFile().getAbsolutePath() + "/slurm/" + moldir.getName() + "_slurm_"+ (i+1) + ".sh");
				PrintWriter pr = new PrintWriter(new BufferedWriter(new FileWriter(file)));
				
				pr.println("#!/bin/zsh");
				pr.println();
				
				pr.println("#SBATCH -J SEPP_" + moldir.getName() + "_" + (i+1));
				pr.println("#SBATCH -o " + SEPPtool.molecule_subdirectory + "/" + moldir.getName() + "/slurm/" + moldir.getName() + "_slurm_" + (i+1) +"_%J.out"); // output-file
				pr.println("#SBATCH -t 24:00:00"); // time hh:mm:ss // TODO: estimate time based on molecule size
				pr.println("#SBATCH --mem-per-cpu=30000"); // memory in MB
				pr.println("#SBATCH -vvv"); // verbosity
				// email notification
				if (i==0) {
					// first
					pr.println("#SBATCH --mail-type=BEGIN,FAIL"); 
				} else if (i<calculationCluster.length-1) {
					//middle
					pr.println("#SBATCH --mail-type=FAIL");
				} else {
					//last
					pr.println("#SBATCH --mail-type=END,FAIL");
				}
				pr.println();
				
				pr.println("module load CHEMISTRY");
				pr.println("module load gaussian/09.c01");
				pr.println("module load turbomole");
				pr.println("module load openbabel");
				pr.println();
				
				String call = "java -jar " + jarFile + " " + moldir.name;
				for (int j = 0; j < calculationCluster[i].length; j++)
				{
					call += " " + calculationCluster[i][j];
				}
				pr.print(call);
				
				if (i < calculationCluster.length - 1)
				{
					pr.print(" && sbatch "+SEPPtool.molecule_subdirectory + "/" + moldir.getName() +"/slurm/" +moldir.getName() + "_slurm_" + (i+2) + ".sh");
				}
				pr.println();
				
				pr.close();
			}
		} catch (IOException e)
		{
			System.out.println("Could not create slurm scripts: " + e.getLocalizedMessage());
			return false;
		}
		
		return true;
	}
	
	public static boolean submitSlurmScripts(MoleculeDirectory moldir)
	{
		String firstScript = moldir.getFile().getAbsolutePath() + "/slurm/" + moldir.getName()+"_slurm_1.sh"; 
		ArrayList<String> alist = new ArrayList<String>();

		// add the list )of commands to the list
		alist.add("sbatch");
		alist.add(firstScript);
		
		ProcessRunner proc = new ProcessRunner(alist);
		try {
			proc.run();
		} catch (IOException|InterruptedException e) {
			log(moldir, "Error submitting slurm script ("+alist.get(0)+"): "+e.getLocalizedMessage());
			return false;
		}	

		if (proc.getExitValue() != 0)
		{
			log(moldir, alist.get(0)+" returned exit value " + proc.getExitValue());
			return false;
		}
		
		return true;
	}
	
	public static void listMolecules()
	{
		for (MoleculeDirectory mol:getAllMolecules())
		{
			System.out.println(mol.getName());
		}
	}
	
	public static Vector<MoleculeDirectory> getAllMolecules()
	{
		Vector<MoleculeDirectory> mols = new Vector<MoleculeDirectory>();
		File molsdir = new File(System.getProperty("user.dir")+"/"+molecule_subdirectory);
		File[] files = molsdir.listFiles(); 
		if (files == null) 
		{
			System.err.println("Directory " + molsdir.getAbsolutePath() + " does not exist. Create it? y/n");
			char ans = scanner.next().charAt(0);
			System.out.println("input: "+ans);
			if ('y'==ans)
			{
				boolean success = molsdir.mkdir();
				if (!success) 
				{
					System.out.println("Could not create directoy " + molsdir.getAbsolutePath());
				}
				else
				{
					System.out.println("Created directory " + molsdir.getAbsolutePath()+ " :: "+molsdir.isDirectory());
				}
			}
			
			// return empty vector
			return mols;
		}
		for (final File moldir:files)
		{
			if (moldir.isDirectory())
				mols.add(new MoleculeDirectory(moldir));
		}
		return mols;
	}
	
	public static void readConfigFile()
	{
		// try loading config file
		File configFile = new File(pathConfigFile);
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(Config.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			config = (Config) jaxbUnmarshaller.unmarshal(configFile);			
		} catch (JAXBException e) {
			// config file does not exist or is erroneous
			System.out.println("Cannot read config file \""+pathConfigFile+"\"");
			// TODO: ask if this should be a new SEPPtool directory and a config file should be created
			createConfigFile();
		}
	}
	
	public static void createConfigFile()
	{
		config = new Config();
		config.readUserValues();
		writeConfigFile();
	}
	
	public static void writeConfigFile()
	{
		try {
			JAXBContext jc = JAXBContext.newInstance(Config.class);
	        Marshaller m = jc.createMarshaller();
	        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	        m.marshal( config, new File(pathConfigFile) );
		} catch (JAXBException e) {
			System.err.println("Could not save config file");
			e.printStackTrace();
		}
	}
	
	public static void log(MoleculeDirectory moldir, String log)
	{
		System.out.println(log);
		PrintWriter pr;
		try {
			pr = new PrintWriter(new BufferedWriter(new FileWriter(moldir.file_log, true)));
			pr.println(
					DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(Date.from(Instant.now()))+": "+log);
			pr.close();
		} catch (IOException e) {
			System.out.println("Could not write to log file " + moldir.file_log.getAbsolutePath());
			e.printStackTrace();
		}
	}
	
	public static boolean perform(Calculation calc, MoleculeDirectory moldir)
	{
		log(moldir, "Start calculation \"" + calc.menuEntryString() + "\"");
		double startTime = System.nanoTime();
		boolean success = calc.perform(moldir);
		String output = "Calculation \"" + calc.menuEntryString() + "\" " + (success ? "successfull" : "failed");
		output += String.format(" in %.3f seconds\n", (System.nanoTime()-startTime)*1e-9);
		log(moldir, output);
		return success;
	}
}
