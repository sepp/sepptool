package SEPPtool.cube;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Vector;

/**
 * Data storage and parsing of a cube file which was generated with gaussian's cubegen
 * @author sebastian
 *
 */
public class Cube
{
	int n_atoms;
	int[] n_steps = new int[3];
	Point firstPoint;
	Point stepSize;
	
	int[] atom_type;
	Point[] atom_position;
	
	double[][][] rho_e;
	
	double rho0;
	Vector<Triangle> triangles;
	
	
	public Cube()
	{
	}
	
	public boolean read(File cubefile) throws FileNotFoundException
	{
		Scanner scanner = new Scanner(cubefile);
		
		// ignore first 2 comment lines
		scanner.nextLine();
		scanner.nextLine();
		
		// third line: number of atoms and position of first point
		n_atoms = scanner.nextInt();
		firstPoint = new Point(scanner);
		
		// lines 4-6
		stepSize = new Point();
		for (int i=0; i<3; i++)
		{
			n_steps[i] = scanner.nextInt();
			
			for (int j = 0; j < 3; j++)
			{
				double x = scanner.nextDouble();
				if (i == j)
				{
					stepSize.set(i, x);
				}
				else
				{
					if (  x != 0.0 ) 
					{
						System.err.println("Cube grid must be in its standard orientation in file "+cubefile.getAbsolutePath());
						scanner.close();
						return false;
					}
				}
			}
		}
		
		// read atoms
		atom_type = new int[n_atoms];
		atom_position = new Point[n_atoms];
		for (int i = 0; i < n_atoms; i++)
		{
			atom_type[i] = scanner.nextInt();
			scanner.nextDouble(); // ignore
			atom_position[i] = new Point(scanner);
		}
		
		// read electron density
		rho_e = new double[n_steps[0]][n_steps[1]][n_steps[2]];
		for (int i = 0; i < n_steps[0]; i++)
		for (int j = 0; j < n_steps[1]; j++)
		for (int k = 0; k < n_steps[2]; k++)
		{
			rho_e[i][j][k] = scanner.nextDouble();
		}
		
		scanner.close();
		return true;
	}
	
	public void constructSurface(double rho0)
	{
		// clear triangles
		triangles = new Vector<Triangle>();
		this.rho0 = rho0;
		
		int n_success=0;
		int n_fail=0;
		
		// loop over all voxels
		for (int i = 0; i < n_steps[0] - 1; i++)
		for (int j = 0; j < n_steps[1] - 1; j++)
		for (int k = 0; k < n_steps[2] - 1; k++)
		{
			Vector<SPoint> spoints = new Vector<SPoint>(12);
			
			// loop over edges of voxel to find points where rho_e = rho0
			for (int l = 0; l < 2; l++)
			for (int m = 0; m < 2; m++)
			{
				// x-direction (i)
				if ( (rho_e[i][j+l][k+m] - rho0) * (rho_e[i+1][j+l][k+m] - rho0) < 0 )
				{
	                double frac = Math.abs( (rho_e[i][j+l][k+m] - rho0) / (rho_e[i][j+l][k+m] - rho_e[i+1][j+l][k+m]));
	                SPoint spoint = new SPoint(
	                		firstPoint.getX() + (i + frac) * stepSize.getX(),
	                		firstPoint.getY() + (j + l) * stepSize.getY(),
	                		firstPoint.getZ() + (k + m) * stepSize.getZ()
	                		);
	                spoint.nextGridPoints[0][0] = i;
	                spoint.nextGridPoints[0][1] = j+l;
	                spoint.nextGridPoints[0][2] = k+m;
	                spoint.nextGridPoints[1][0] = i+1;
	                spoint.nextGridPoints[1][1] = j+l;
	                spoint.nextGridPoints[1][2] = k+m;
	                // remember the two faces of the cube which the spoint touches
	                // 0: normal vector not in that direction
	                // -1: normal vector in negative direction
	                // +1: normal vector in positive direction
	                spoint.face[0] = 0;				// x
	                spoint.face[1] = (l * 2) -1;	// y
	                spoint.face[2] = (m * 2) -1;	// z
	                
	                spoints.add(spoint);
				}
				
				// y-direction (j)
				if ( (rho_e[i+l][j][k+m] - rho0) * (rho_e[i+l][j+1][k+m] - rho0) < 0 )
				{
	                double frac = Math.abs( (rho_e[i+l][j][k+m] - rho0) / (rho_e[i+l][j][k+m] - rho_e[i+l][j+1][k+m]));
	                SPoint spoint = new SPoint(
	                		firstPoint.getX() + (i + l) * stepSize.getX(),
	                		firstPoint.getY() + (j + frac) * stepSize.getY(),
	                		firstPoint.getZ() + (k + m) * stepSize.getZ()
	                		);
	                spoint.nextGridPoints[0][0] = i+l;
	                spoint.nextGridPoints[0][1] = j;
	                spoint.nextGridPoints[0][2] = k+m;
	                spoint.nextGridPoints[1][0] = i+l;
	                spoint.nextGridPoints[1][1] = j+1;
	                spoint.nextGridPoints[1][2] = k+m;
	                // remember the two faces of the cube which the spoint touches
	                // 0: normal vector not in that direction
	                // -1: normal vector in negative direction
	                // +1: normal vector in positive direction
	                spoint.face[0] = (l * 2) - 1;	// x
	                spoint.face[1] = 0;				// y
	                spoint.face[2] = (m * 2) - 1;	// z
	                
	                spoints.add(spoint);
				}
				
				// z-direction (k)
				if ( (rho_e[i+l][j+m][k] - rho0) * (rho_e[i+l][j+m][k+1] - rho0) < 0 )
				{
	                double frac = Math.abs( (rho_e[i+l][j+m][k] - rho0) / (rho_e[i+l][j+m][k] - rho_e[i+l][j+m][k+1]));
	                SPoint spoint = new SPoint(
	                		firstPoint.getX() + (i + l) * stepSize.getX(),
	                		firstPoint.getY() + (j + m) * stepSize.getY(),
	                		firstPoint.getZ() + (k + frac) * stepSize.getZ()
	                		);
	                spoint.nextGridPoints[0][0] = i+l;
	                spoint.nextGridPoints[0][1] = j+m;
	                spoint.nextGridPoints[0][2] = k;
	                spoint.nextGridPoints[1][0] = i+l;
	                spoint.nextGridPoints[1][1] = j+m;
	                spoint.nextGridPoints[1][2] = k+1;
	                // remember the two faces of the cube which the spoint touches
	                // 0: normal vector not in that direction
	                // -1: normal vector in negative direction
	                // +1: normal vector in positive direction
	                spoint.face[0] = (l * 2) - 1;	// x
	                spoint.face[1] = (m * 2) - 1;	// y
	                spoint.face[2] = 0;	// z
	                
	                spoints.add(spoint);
				}
			} // done finding points at edge of voxel
			
			int n = spoints.size();
			
			if (n > 0)
			{
				n_success++; // decrease later if this fails
				
				Triangle triangle;
				while ((triangle = findIsolatedCorner(i, j, k, spoints)) != null)
				{
					triangles.add(triangle);
					spoints.removeAll(Arrays.asList(triangle.getPoints()));
				}
				if (spoints.size()==0) continue; // continue with next voxel
				
				Vector<SPoint> points;
				while ((points = findIsolatedEdge(i, j, k, spoints)) != null)
				{
					Vector<Triangle> tri = createTriangles(points); 
					if (tri != null)
					{
						triangles.addAll(tri);
						spoints.removeAll(points);
					} else break;
					
				}
				if (spoints.size()==0) continue; // continue with next voxel
				
				int[][] face_count = getFaceCount(spoints);
				int max_face_count = getMaxFaceCount(face_count);
				if (max_face_count == 2)
				{
					Vector<Triangle> tri = createTriangles(spoints); 
					if (tri != null)
					{
						triangles.addAll(tri);
					}
					else 
					{
						n_success--;
						n_fail++;
					}
				}
				else
				{
					//System.out.println("# Remaining Spoints: "+spoints.size()+" max_face_count="+max_face_count);
					n_success--;
					n_fail++;
				}
				
				
			}
			
		} // done looping over grid points
		if (n_fail>0)
		{
			System.out.println("successful voxels: "+n_success);
			System.out.println("failed voxels: "+n_fail);
		}
	}
	
	/**
	 * 
	 * @param i lowest x-index of voxel
	 * @param j lowest y-index of voxel
	 * @param k lowest z-index of voxel
	 */
	public static Triangle findIsolatedCorner(int i, int j, int k, Vector<SPoint> spoints)
	{
		// the Vector which will be returned
		Vector<SPoint> ret;
		
		for (int l = 0; l < 2; l++)
		for (int m = 0; m < 2; m++)
		for (int o = 0; o < 2; o++)
		{
			ret = SPoint.getSPointsNextToGridPoint(i + l, j + m, k + o, spoints);
			if (ret.size() == 3) return new Triangle(ret);
		}
		
		return null; // no isolated triangle found
	}
	
	/**
	 * 
	 * @param i lowest x-index of voxel
	 * @param j lowest y-index of voxel
	 * @param k lowest z-index of voxel
	 */
	public Vector<SPoint> findIsolatedEdge(int i, int j, int k, Vector<SPoint> spoints)
	{
		for (int l = 0; l < 2; l++)
		for (int m = 0; m < 2; m++)
		for (int direction = 0; direction < 3; direction++)
		{
			int corner[][] = new int[2][3];
			
			// 0 l l
			// l 0 m
			// m m 0
			corner[0][0] = i + ( (direction==0) ? 0 : l );
			corner[0][1] = j + ( (direction==1) ? 0 : ((direction == 0) ? l : m) );
			corner[0][2] = k + ( (direction==2) ? 0 : m );
			
			corner[1][0] = i + ( (direction==0) ? 1 : l );
			corner[1][1] = j + ( (direction==1) ? 1 : ((direction == 0) ? l : m) );
			corner[1][2] = k + ( (direction==2) ? 1 : m );

			Vector<SPoint> p1 = SPoint.getSPointsNextToGridPoint(corner[0][0], corner[0][1], corner[0][2], spoints);
			Vector<SPoint> p2 = SPoint.getSPointsNextToGridPoint(corner[1][0], corner[1][1], corner[1][2], spoints);
			
			// both corners have 2 neighbors
			if (p1.size() == 2 && p2.size() == 2)
			{
				boolean sameEdge = false;
				// any of these points are on the same edge? If so, one corner is below and the other corner above rho0
				for (SPoint sp1:p1)	for (SPoint sp2:p2)
				{
					// all faces are the same => points are on the same edge
					if (sp1.face[0] == sp2.face[0] && sp1.face[1] == sp2.face[1] && sp1.face[2] == sp2.face[2]) sameEdge = true;
				}
				if (!sameEdge)
				{	// no 2 points on the same edge => found an isolated edge
					Vector<SPoint> ret = new Vector<SPoint>();
					ret.addAll(p1);
					ret.addAll(p2);
					return ret;
				}
			}
			
		}
		
		return null;
	}
	
	/**
	 * Returns the number of SPoints that lie on the edges of a face in a 3x2 array.
	 * First index: 0 = x-direction, 1 = y-direction, 2 = z-direction.
	 * Second index: 0 = negative direction, 1 = positive direction
	 * @param spoints
	 * @return
	 */
	public int[][] getFaceCount(Vector<SPoint> spoints)
	{
		int[][] face_count = new int[3][2];
		
        for (int l = 0; l < 3; l++) for (int m = 0; m < 2; m++) face_count[l][m]=0;
            
        for (int i = 0; i < spoints.size(); i++)
        {
            for (int m = 0; m < 3; m++)
            {
                if (spoints.get(i).face[m]==1)
                {
                	face_count[m][1]++;
                }
                else if (spoints.get(i).face[m]==-1)
                {
                	face_count[m][0]++;
                }
            }
        }
		
		return face_count;
	}
	
	/**
	 * 
	 * @param face_count
	 * @return the maximum number of SPoints an a face
	 */
	public int getMaxFaceCount(final int[][] face_count)
	{
		int max=0;
		for (int i = 0; i < face_count.length; i++)
		for (int j = 0; j < face_count[i].length; j++)
		{
			if (face_count[i][j]>max) max = face_count[i][j];
		}
		return max;
	}
	
	public Vector<Triangle> createTriangles(final Vector<SPoint> spoints)
	{
		Vector<Triangle> l_triangles = new Vector<Triangle>();
		if (spoints.size()<3) return l_triangles;
		else if (spoints.size()==3)
		{
			Triangle tri = new Triangle(spoints);
			l_triangles.add(tri);
			return l_triangles;
		}
		
		// create a copy
		Vector<SPoint> orig = new Vector<SPoint>(spoints);
		
		Vector<SPoint> sorted = new Vector<SPoint>(spoints.size());
		
		sorted.add(orig.get(0));
		orig.remove(0);
		
		
		while (!orig.isEmpty())
		{
			boolean foundPartner = false;

			for (int i = 0; i < orig.size(); i++)
			{
				SPoint sp = orig.get(i);
				if (sorted.lastElement().onSameFaceAs(sp))
				{
					sorted.add(sp);
					orig.remove(sp);
					foundPartner = true;
					break;
				}
			}
			if (!foundPartner)
			{
				// something went wrong
				System.out.println("Something went wrong creating triangles with "+spoints.size()+" SPoints");
				for (SPoint spo:spoints)
					System.out.println("  "+spo.getX()+", "+spo.getY()+", "+spo.getZ()+" face "+spo.face[0]+", "+spo.face[1]+", "+spo.face[2]
							+" rho_e "+rho_e[spo.nextGridPoints[0][0]][spo.nextGridPoints[0][1]][spo.nextGridPoints[0][2]]+ " "
									  +rho_e[spo.nextGridPoints[1][0]][spo.nextGridPoints[1][1]][spo.nextGridPoints[1][2]]);
				return null;
			}
		}
		
		if (!sorted.lastElement().onSameFaceAs(sorted.firstElement()))
		{
			System.out.println("Something went wrong creating triangles with "+spoints.size()+" SPoints: last SPoint in sorted list is not on same face as first SPoint");
			for (SPoint spo:sorted)
				System.out.println("  "+spo.getX()+", "+spo.getY()+", "+spo.getZ()+" face "+spo.face[0]+", "+spo.face[1]+", "+spo.face[2]
						+" rho_e "+rho_e[spo.nextGridPoints[0][0]][spo.nextGridPoints[0][1]][spo.nextGridPoints[0][2]]+ " "
								  +rho_e[spo.nextGridPoints[1][0]][spo.nextGridPoints[1][1]][spo.nextGridPoints[1][2]]);
			return null;
		}
		
		// remove points with smallest distance between neighboring points until 3 points remain
		while (sorted.size()>3)
		{
			int size = sorted.size();
			int index_smallest_distance=0;
			double smallest_distance = Double.MAX_VALUE;
			for (int i = 0; i < size; i++)
			{
				double distance = sorted.get( i == 0 ? size - 1 : i - 1 ).distanceTo( sorted.get( i == size - 1 ? 0 : i + 1 ) );
				if (distance < smallest_distance)
				{
					smallest_distance = distance;
					index_smallest_distance = i;
				}
			}
			
			Triangle triangle = new Triangle(
					sorted.get( index_smallest_distance == 0 ? size - 1 : index_smallest_distance - 1 ),
					sorted.get(index_smallest_distance),
					sorted.get( index_smallest_distance == size - 1 ? 0 : index_smallest_distance + 1 )
					);
			l_triangles.add(triangle);
			sorted.removeElementAt(index_smallest_distance); // remove element which was the "center" of the path
		}
		// exactly 3 points remain
		l_triangles.add(new Triangle(sorted));
		
		return l_triangles;
	}

	public double surfaceArea()
	{
		double area = 0.0;
		for (Triangle tri : triangles)
		{
			area += tri.area();
		}
		return area;
	}
	
	public double volume()
	{
		// count number of grid points that lie within geometry 
		int n=0;
	    for (int i=0; i<n_steps[0]; i++)
	    for (int j=0; j<n_steps[1]; j++)
	    for (int k=0; k<n_steps[2]; k++)
	    {
	        if (rho_e[i][j][k] > rho0) n++;
	    }
	    
	    // multiply number of grid points that lie within geometry with size of a voxel
	    return n
	           * stepSize.get(0)
	           * stepSize.get(1)
	           * stepSize.get(2);
	}
	
	public double meanRadiusOfCurvature()
	{
		int numberOfSteps=20;
		
	    double rc=0;

	    for (int i=0; i<2*numberOfSteps; i++)
	    {
	    	double phi = ((double)i)/((double)numberOfSteps)*Math.PI;
			for (int j=0; j<numberOfSteps; j++)
			{
			    double theta = Math.acos(1-2*((double)(2*j+1))/((double)(numberOfSteps*2)));
			    
			    Point direction = new Point();
			    direction.set(0, Math.sin(theta)*Math.cos(phi) );
			    direction.set(1, Math.sin(theta)*Math.sin(phi) );
			    direction.set(2, Math.cos(theta) );
			    
			    rc += contactDistance(direction);
			}
	    }
	    
	    return(rc/(2*numberOfSteps*numberOfSteps));
	}
	
	public double contactDistance(Point direction)
	{
	    double H_max=0.0;
	    
	    // for each point belonging to each triangle
	    for (Triangle triangle : triangles)
	    for (Point point : triangle.getPoints())
	    {
	    	// calculate scalar product
	    	double H = 0.0;
	    	for (int i = 0; i < 3; i++) H += point.get(i) * direction.get(i);
	    	
  	        if (H > H_max) H_max = H;
	    }
	    
	    return H_max;
	}
	
	public double nonsphericityFactor()
	{
		double S = surfaceArea();
		double V = volume();
		double Rc = meanRadiusOfCurvature();
		return Rc * S / 3.0 / V;
	}

	public double numberOfElectrons(double rho_e0)
	{
	    double n = 0.0;
	    for (int i = 0; i < n_steps[0]; i++)
	    for (int j = 0; j < n_steps[1]; j++)
	    for (int k = 0; k < n_steps[2]; k++)
	    {   
	    	if (rho_e[i][j][k] > rho0)
	    		n += rho_e[i][j][k] * stepSize.getX() * stepSize.getY() * stepSize.getZ();
	    }
	    
	    return n;
	}
}
