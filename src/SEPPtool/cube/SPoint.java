package SEPPtool.cube;

import java.util.Vector;

/**
 * Represents a point at a grid position
 * @author sebastian
 *
 */
public class SPoint extends Point
{
	public int nextGridPoints[][] = new int[2][3];
	public int face[] = new int[3];
	
	public SPoint(double x, double y, double z) {
		super(x,y,z);
	}
	
	public boolean onSameFaceAs(SPoint partner)
	{
		// Both SPoints share a face if for one direction both faces are -1 or both are 1 -> product of faces are 1
		for (int i = 0; i < 3; i++)
		{
			if (face[i] * partner.face[i] == 1) return true;
		}
		return false;
	}
	
	/**
	 * Check whether given grid point is on the edge on whoch this SPoint lies on.
	 * @param i x-index of grid point
	 * @param j y-index of grid point
	 * @param k z-index of grid point
	 * @return
	 */
	public boolean isNextGridPoint(int i, int j, int k)
	{
		return (
				nextGridPoints[0][0] == i &&
				nextGridPoints[0][1] == j &&
				nextGridPoints[0][2] == k
				) || (
				nextGridPoints[1][0] == i &&
				nextGridPoints[1][1] == j &&
				nextGridPoints[1][2] == k
				);
	}
	
	/**
	 * Returns all SPoints that lie on an edge that is connected to the given grid point
	 * @param i x-index of grid point
	 * @param j y-index of grid point
	 * @param k z-index of grid point
	 * @param spoints all SPoint to consider in the search
	 * @return
	 */
	public static Vector<SPoint> getSPointsNextToGridPoint(int i, int j, int k, Vector<SPoint> spoints)
	{
		// the Vector which will be returned
		Vector<SPoint> ret = new Vector<SPoint>();
		
		for (SPoint spoint : spoints)
			if (spoint.isNextGridPoint(i, j, k))
				ret.add(spoint);
		
		return ret;
	}
}