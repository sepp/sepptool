package SEPPtool.cube;

import java.util.Scanner;

/**
 * Convenience class to represent a 3d point
 * @author sebastian
 *
 */
public class Point
{
	double[] coord = new double[3];
			
	public Point()
	{
		this(0.0, 0.0, 0.0);
	}
	
	public Point(double x, double y, double z)
	{
		this.coord[0] = x;
		this.coord[1] = y;
		this.coord[2] = z;
	}
	
	public Point(Scanner scanner)
	{
		read(scanner);
	}
	
	/**
	 * Reads next 3 double values from given Scanner and assigns them to the x, y, and z coordinate of this Point
	 * @param scanner
	 */
	public void read(Scanner scanner)
	{
		coord[0] = scanner.nextDouble();
		coord[1] = scanner.nextDouble();
		coord[2] = scanner.nextDouble();
	}
	
	public void set(int i, double val)
	{
		if (i>=0 && i<3) coord[i] = val;
	}
	
	public double get(int i)
	{
		if (i>=0 && i<3)  return coord[i];
		else return Double.NaN;
	}
	
	public double getX() { return get(0); }
	public double getY() { return get(1); }
	public double getZ() { return get(2); }
	
	public double distanceTo(Point partner)
	{
		return Math.sqrt(
					Math.pow(getX() - partner.getX(), 2) +
					Math.pow(getY() - partner.getY(), 2) +
					Math.pow(getZ() - partner.getZ(), 2) );
	}
	
	public void print()
	{
		System.out.printf("(%10.6g %10.6g %10.6g)\n", coord[0], coord[1], coord[2]);
	}
	
	public static Point zero = new Point(0,0,0);
}