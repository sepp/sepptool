package SEPPtool.cube;

import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Data storage for a cube table and its entries
 * @author sebastian
 *
 */
@XmlRootElement
public class CubeTable {
	
	double numberOfElectrons;
	Vector<CubeTableEntry> cubeTableEntry = new Vector<CubeTableEntry>();
	
	public CubeTable()
	{
		this(Double.NaN);
	}
	
	public CubeTable(double numberOfElectrons)
	{
		this.numberOfElectrons = numberOfElectrons;
	}
	
	public double getNumberOfElectrons() {
		return numberOfElectrons;
	}
	public void setNumberOfElectrons(double numberOfElectrons) {
		this.numberOfElectrons = numberOfElectrons;
	}
	public Vector<CubeTableEntry> getCubeTableEntry() {
		return cubeTableEntry;
	}
	public void setCubeTableEntry(Vector<CubeTableEntry> cubeTableEntry) {
		this.cubeTableEntry = cubeTableEntry;
	}

	public void addCubeTableEntry(double surfaceArea, double volume, double rc, double ne)
	{
		cubeTableEntry.add(new CubeTableEntry(surfaceArea, volume, rc, ne, numberOfElectrons));
	}
	
	/**
	 * Return entry with interpolated values for given electron percentage
	 * @param percentage
	 * @return
	 */
	public CubeTableEntry getCubeTableEntryAtEnclosedElectronPercentage(double percentage)
	{
		Collections.sort(cubeTableEntry, new Comparator<CubeTableEntry>() {
			@Override
			public int compare(CubeTableEntry arg0, CubeTableEntry arg1) {
				if (arg0.getNumberOfEnclosedElectrons() < arg1.getNumberOfEnclosedElectrons()) return -1;
				else if (arg0.getNumberOfEnclosedElectrons() > arg1.getNumberOfEnclosedElectrons()) return 1;
				else return 0;
			}
		});
		
		CubeTableEntry entry1=null, entry2=null;
		for (int i = 0; i < cubeTableEntry.size()-1; i++)
		{
			if (percentage > cubeTableEntry.get(i).getNumberOfEnclosedElectrons()/numberOfElectrons &&
				percentage < cubeTableEntry.get(i+1).getNumberOfEnclosedElectrons()/numberOfElectrons)
			{
				entry1 = cubeTableEntry.get(i);
				entry2 = cubeTableEntry.get(i+1);
				break;
			}
		}
		if (entry1==null || entry2==null) return null;
		
		double fraction = (percentage*numberOfElectrons - entry1.getNumberOfEnclosedElectrons()) /
				(entry2.getNumberOfEnclosedElectrons() - entry1.getNumberOfEnclosedElectrons());
		
		double surfaceArea = entry1.getSurfaceArea() + fraction * (entry2.getSurfaceArea() - entry1.getSurfaceArea());
		double volume = entry1.getVolume() + fraction * (entry2.getVolume() - entry1.getVolume());
		double rc = entry1.getMeanRadiusOfCurvature() + fraction * (entry2.getMeanRadiusOfCurvature() - entry1.getMeanRadiusOfCurvature());
		double ne = entry1.getNumberOfEnclosedElectrons() + fraction * (entry2.getNumberOfEnclosedElectrons() - entry1.getNumberOfEnclosedElectrons());
		
		return new CubeTableEntry(surfaceArea, volume, rc, ne, numberOfElectrons);
	}

	public static class CubeTableEntry
	{
		double surfaceArea;
		double volume;
		double meanRadiusOfCurvature;
		double numberOfEnclosedElectrons;
		double totalNumberOfElectrons;
		
		// constructor with no arguments needed for JAXB
		@SuppressWarnings("unused")
		private CubeTableEntry() { }
		
		public CubeTableEntry(double surfaceArea, double volume, double rc, double ne, double tne) {
			this.surfaceArea = surfaceArea;
			this.volume = volume;
			this.meanRadiusOfCurvature = rc;
			this.numberOfEnclosedElectrons = ne;
			this.totalNumberOfElectrons = tne;
		}
		
		public double getSurfaceArea() {
			return surfaceArea;
		}
		public void setSurfaceArea(double surfaceArea) {
			this.surfaceArea = surfaceArea;
		}
		public double getVolume() {
			return volume;
		}
		public void setVolume(double volume) {
			this.volume = volume;
		}
		public double getMeanRadiusOfCurvature() {
			return meanRadiusOfCurvature;
		}
		public void setMeanRadiusOfCurvature(double meanRadiusOfCurvature) {
			this.meanRadiusOfCurvature = meanRadiusOfCurvature;
		}
		public double getNumberOfEnclosedElectrons() {
			return numberOfEnclosedElectrons;
		}
		public void setNumberOfEnclosedElectrons(double numberOfEnclosedElectrons) {
			this.numberOfEnclosedElectrons = numberOfEnclosedElectrons;
		}
		
		
		public double getNonsphericity()
		{
			return meanRadiusOfCurvature * surfaceArea / 3.0 / volume;
		}
		
		public double getPercentageOfEnclosedElectrons()
		{
			return numberOfEnclosedElectrons / totalNumberOfElectrons;
		}
	}
}
