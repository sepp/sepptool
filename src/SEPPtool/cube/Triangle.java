package SEPPtool.cube;

import java.util.Vector;

/**
 * Representation of a triangle with 3 Point objects
 * @author sebastian
 *
 */
public class Triangle
{
	Point spoints[] = new Point[3];
	
	public Triangle(Point p1, Point p2, Point p3)
	{
		spoints[0] = p1;
		spoints[1] = p2;
		spoints[2] = p3;
		if (p1 == null || p2 == null || p3 == null) throw new NullPointerException();
	}
	
	public Triangle(Vector<SPoint> ret)
	{
		if (ret.size()==3)
		{
			for (int i = 0; i < 3; i++)
			{
				spoints[i] = ret.get(i);
				if (spoints[i] == null) throw new NullPointerException();
			}
		}
		else
		{
			System.err.println("Trying to create triangle with more than 3 points");
			throw new NullPointerException(); // TODO: hi-jacked NullPointerException; create new exception / use Exception?
		}
	}
	
	public Point[] getPoints()
	{
		return spoints;
	}
	
	public double area()
	{
		double[] AB = new double[3]; // vector from first to second point
		double[] AC = new double[3]; // vector from first to thirs point
		for (int i = 0; i < 3; i++) 
		{
			AB[i] = spoints[1].get(i) - spoints[0].get(i);
			AC[i] = spoints[2].get(i) - spoints[0].get(i);
		}
		
		double[] c = new double[3];
		// cross product
	    c[0] = AB[1]*AC[2] - AB[2]*AC[1];
	    c[1] = AB[2]*AC[0] - AB[0]*AC[2];
	    c[2] = AB[0]*AC[1] - AB[1]*AC[0];
		
	    // area is half of the length of the cross product
		return 0.5 * Math.sqrt(c[0]*c[0] + c[1]*c[1] + c[2]*c[2]);
	}
}
