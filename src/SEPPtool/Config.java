package SEPPtool;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Stores paths to programs and directories.
 * @author sebastian
 *
 */
@XmlRootElement(name="SEPPtool")
public class Config {

	// paths to executables and directory
	String pathObabel=null;				// open babel executable
	String pathGaussian=null;			// gaussian executable
	String pathGaussExedir=null;		// gaussian's EXEDIR
	String pathGaussScrdir=null;		// gaussian's scratch dir
	String pathFormchk=null;			// gaussian's formchk executable
	String pathCubegen=null;			// gaussian's cubegen executable
	String pathTurbomoleDefine=null;	// turbomole's define executable
	String pathTurbomoleDSCF=null;		// turbomole's dscf executable
	String pathTurbomoleESCF=null;		// turbomole's escf executable
	String pathTurbomoleX2T=null;		// turbomole's x2t script
	
	public String getPathObabel() {
		return pathObabel;
	}
	public void setPathObabel(String pathObabel) {
		this.pathObabel = pathObabel;
	}
	public String getPathGaussian() {
		return pathGaussian;
	}
	public void setPathGaussian(String pathGaussian) {
		this.pathGaussian = pathGaussian;
	}
	public String getPathGaussExedir() {
		return pathGaussExedir;
	}
	public void setPathGaussExedir(String pathGaussExedir) {
		this.pathGaussExedir = pathGaussExedir;
	}
	public String getPathGaussScrdir() {
		return pathGaussScrdir;
	}
	public void setPathGaussScrdir(String pathGaussScrdir) {
		this.pathGaussScrdir = pathGaussScrdir;
	}
	public String getPathFormchk() {
		return pathFormchk;
	}
	public void setPathFormchk(String pathFormchk) {
		this.pathFormchk = pathFormchk;
	}
	public String getPathCubegen() {
		return pathCubegen;
	}
	public void setPathCubegen(String pathCubegen) {
		this.pathCubegen = pathCubegen;
	}
	public String getPathTurbomoleDefine() {
		return pathTurbomoleDefine;
	}
	public void setPathTurbomoleDefine(String pathTurbomoleDefine) {
		this.pathTurbomoleDefine = pathTurbomoleDefine;
	}
	public String getPathTurbomoleDSCF() {
		return pathTurbomoleDSCF;
	}
	public void setPathTurbomoleDSCF(String pathTurbomoleDSCF) {
		this.pathTurbomoleDSCF = pathTurbomoleDSCF;
	}
	public String getPathTurbomoleESCF() {
		return pathTurbomoleESCF;
	}
	public void setPathTurbomoleESCF(String pathTurbomoleESCF) {
		this.pathTurbomoleESCF = pathTurbomoleESCF;
	}
	public String getPathTurbomoleX2T() {
		return pathTurbomoleX2T;
	}
	public void setPathTurbomoleX2T(String pathTurbomoleX2T) {
		this.pathTurbomoleX2T = pathTurbomoleX2T;
	}
	
	/**
	 * Default values are tuned to work with RWTH's high performance cluster
	 */
	public void readUserValues()
	{
		BufferedReader r=new BufferedReader(new InputStreamReader(System.in));
		setPathObabel(readUserValue("obabel", pathObabel==null ? "obabel" : pathObabel, r));
		setPathGaussian(readUserValue("g09 executable", pathGaussian==null ? "g09" : pathGaussian, r));
		setPathGaussExedir(readUserValue("gaussian executable directory (GAUSS_EXEDIR)", pathGaussExedir==null ? "/usr/local_rwth/sw/gaussian/LINUX_emt64/g09.c01" : pathGaussExedir, r));
		setPathGaussScrdir(readUserValue("gaussian scratch directory (GAUSS_SCRDIR)", pathGaussScrdir==null ? "/hpcwork/ab123456" : pathGaussScrdir, r));
		setPathFormchk(readUserValue("formchk executable",pathFormchk==null ? "formchk" : pathFormchk, r));
		setPathCubegen(readUserValue("cubegen executable",pathCubegen==null ? "cubegen" : pathCubegen,r));
		setPathTurbomoleDefine(readUserValue("turbomole define executable",pathTurbomoleDefine==null ? "define" : pathTurbomoleDefine, r));
		setPathTurbomoleDSCF(readUserValue("turbomole dscf executable",pathTurbomoleDSCF==null ? "dscf" : pathTurbomoleDSCF,r));
		setPathTurbomoleESCF(readUserValue("turbomole escf executable",pathTurbomoleESCF==null ? "escf" : pathTurbomoleESCF,r));
		setPathTurbomoleX2T(readUserValue("turbomole x2t executable",pathTurbomoleX2T==null ? "x2t" : pathTurbomoleX2T,r));
	}	
	
	/**
	 * 
	 * @param prompt which file/path is being read
	 * @param defaultValue a given default, which the user can accept by just hitting enter
	 * @param r the input reader
	 * @return
	 */
	private String readUserValue(String prompt, String defaultValue, BufferedReader r)
	{
		System.out.println("Enter path to "+prompt+" ["+defaultValue+"]");
		String line;
		int n_tries = 10; // try reading input 10 times
		for (int i = 0; i < 10; i++)
		{
			try {
				line = r.readLine();
				// success -> return the line (or default value if line is empty)
				if ("".equals(line)) return defaultValue;
				else return line;
			} catch (IOException e) {
				System.err.println("Could not read input, try again");
			}
		}
		System.err.println("Reading input failed " + n_tries + " times, not trying anymore.");
		return "ERROR";
	}
}
