package SEPPtool;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

/**
 * A convenience class to run a process via ProcessBuilder, provide
 * input stream and catch output and error stream.
 * @author sebastian
 *
 */
public class ProcessRunner {
	
	ArrayList<String> out = new ArrayList<String>();
	ArrayList<String> err = new ArrayList<String>();
	int exitValue;
	
	ProcessBuilder builder;

	public ProcessRunner(ArrayList<String> cmd)
	{
		// initialize the processbuilder
		builder = new ProcessBuilder(cmd);		
	}
	
	public ProcessRunner(String cmd)
	{
		this(cmd, null);
	}
	
	public ProcessRunner(String cmd, File workingDir)
	{
		builder = new ProcessBuilder(cmd);
		builder.directory(workingDir);
	}
	
	public void putEnvironmentVariable(String key, String value)
	{
		builder.environment().put(key, value); 
		
	}
	
	public void run() throws IOException, InterruptedException
	{
		run(null, null);
	}
	
	public void run(ArrayList<String> in) throws IOException, InterruptedException
	{
		run(in, null);
	}
	
	public void run(File file_out) throws IOException, InterruptedException
	{
		run(null, file_out);
	}
		
	public void run(ArrayList<String> in, File file_out) throws IOException, InterruptedException
	{
		if (file_out != null) builder.redirectOutput(file_out);
		
		// start the process
		Process process = builder.start();
		
		BufferedReader reader_out = null;
		if (file_out == null) reader_out = new BufferedReader(new InputStreamReader(process.getInputStream()));
		BufferedReader reader_err = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		
		if (in != null)
		{
			BufferedWriter writer_in = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
			for (String line:in)
			{
				writer_in.write(line);
				writer_in.newLine();
			}
			//writer_in.flush();
			writer_in.close();
		}

        String line;
        if (file_out == null) while ((line = reader_out.readLine()) != null) out.add(line);
        while ((line = reader_err.readLine()) != null) err.add(line);
        
        process.waitFor();
        
        exitValue = process.exitValue();
        
        //System.out.println("Done calling "+builder.toString()+", return value: " + exitValue);
	}
	
	public ArrayList<String> getOut()
	{
		return out;
	}

	public ArrayList<String> getErr()
	{
		return err;
	}
	
	public int getExitValue()
	{
		return exitValue;
	}
}
