package SEPPtool;

import java.io.File;

/**
 * This class defines the folder structure for each molecule's directory
 * @author sebastian
 *
 */
public class MoleculeDirectory {
	
	File moldir;
	String name;
	
	final public File file_gauss_geo_com; // input file for geometry optimization
	final public File file_gauss_geo_log; // output file for geometry optimization
	final public File file_gauss_polar_com; // input file for polar calculations
	final public File file_gauss_polar_log; // output file for polar calculations
	final public File file_cubedir; // directory for electron density cube file calculations
	final public File file_cubefile; // cube file
	final public File file_cubetablelegacy; // LEGACY
	final public File file_cubetable; // table with geometric properties at given electron density values
	final public File file_chk; // checkpoint file
	final public File file_fchk; // formatted checkpoint file
	final public File file_turbomoledir; // directory for turbomole calculations
	final public File file_interactiondata; // result file for interactions
	final public File file_resultsnonassoc; // result file for non-association parameters (contains interactiondata and cubetable)
	final public File file_cosmo; // cosmo file
	final public File file_results; // LEGACY
	final public File file_log; // log file for all calculations
	
	public MoleculeDirectory(String name)
	{
		this(new File(System.getProperty("user.dir")+"/"+SEPPtool.molecule_subdirectory+"/"+name));
	}
	
	public MoleculeDirectory(File file)
	{
		this.moldir = file;
		name = file.getName();
		
		file_gauss_geo_com = new File(file.getAbsolutePath() + "/" + name + "_0.com");
		file_gauss_geo_log = new File(file.getAbsolutePath() + "/" + name + "_0.log");
		file_gauss_polar_com = new File(file.getAbsolutePath() + "/" + name + "_0_polar.com");
		file_gauss_polar_log = new File(file.getAbsolutePath() + "/" + name + "_0_polar.log");
		file_cubedir = new File(file.getAbsolutePath() + "/cube_" + name);
		file_cubefile = new File(file_cubedir.getAbsolutePath() + "/" + name + "100.cube");
		file_cubetable = new File(file_cubedir.getAbsolutePath() + "/table_" + name + "_0.xml");
		file_cubetablelegacy = new File(file_cubedir.getAbsolutePath() + "/Tabelle100.txt");
		file_chk = new File(file_cubedir.getAbsolutePath() + "/" + name + "_0_polar.chk");
		file_fchk = new File(file_cubedir.getAbsolutePath() + "/" + name + "_0_polar.fchk");
		file_turbomoledir = new File(file.getAbsolutePath() + "/" + name + "_0");
		file_interactiondata = new File(file.getAbsolutePath() + "/" + name + "_0_interaction.xml");
		file_resultsnonassoc = new File(file.getAbsolutePath() + "/" + name + "_0_nonassoc.xml");
		file_cosmo = new File(file.getAbsolutePath() + "/" + name + "_c0.cosmo");
		file_results = new File(file.getAbsolutePath() + "/finalResults.txt");
		file_log = new File(file.getAbsolutePath() + "/SEPPtool.log");
	}
	
	public String getName()
	{
		return name;
	}
	
	public File getFile()
	{
		return moldir;
	}
	
	public void printFileInfo()
	{
		printFileStatus(file_gauss_geo_com);
		printFileStatus(file_gauss_geo_log);
		printFileStatus(file_gauss_polar_com);
		printFileStatus(file_gauss_polar_log);
		printFileStatus(file_cubedir);
		printFileStatus(file_cubefile);
		printFileStatus(file_cubetable);
		if (SEPPtool.LEGACY) printFileStatus(file_cubetablelegacy);
		printFileStatus(file_chk);
		printFileStatus(file_fchk);
		printFileStatus(file_turbomoledir);
		printFileStatus(file_interactiondata);
		printFileStatus(file_cosmo);
		printFileStatus(file_results);
	}
	
	public void printFileStatus(File file)
	{
		System.out.printf("%-" + (name.length()+20) + "s [%b]\n", file.getName(), file.exists());
	}
	
	@Deprecated
	public boolean isStartingGeometryGiven()
	{
		return file_gauss_geo_com.isFile() || file_cosmo.isFile(); // TODO: check for valid content
	}
}