package SEPPtool.calculations;

import java.io.IOException;
import java.util.ArrayList;

import SEPPtool.Calculation;
import SEPPtool.MoleculeDirectory;
import SEPPtool.ProcessRunner;
import SEPPtool.SEPPtool;

/**
 * Generate a cube file with gaussian's cubegen
 * @author sebastian
 *
 */
public class Cubegen implements Calculation {

	@Override
	public boolean calculationAlreadyPerformed(MoleculeDirectory moldir) {
		return moldir.file_cubefile.isFile(); // TODO: check for valid content
	}

	@Override
	public boolean calculationReadyToPerform(MoleculeDirectory moldir) {
		return moldir.file_fchk.isFile(); // TODO check content
	}

	@Override
	public boolean perform(MoleculeDirectory moldir) {
		
		ArrayList<String> alist = new ArrayList<String>();

		// add the list of commands to the list
		alist.add(SEPPtool.config.getPathCubegen());
		alist.add("1");
		alist.add("density=SCF");
		alist.add(moldir.file_fchk.getAbsolutePath());
		alist.add(moldir.file_cubefile.getAbsolutePath());
		alist.add("100"); // level
		
		ProcessRunner proc = new ProcessRunner(alist);
		proc.putEnvironmentVariable("GAUSS_EXEDIR", SEPPtool.config.getPathGaussExedir());
		proc.putEnvironmentVariable("GAUSS_SCRDIR", SEPPtool.config.getPathGaussScrdir());
		try {
			proc.run();
		} catch (IOException|InterruptedException e) {
			System.out.println("Error calling cubegen ("+alist.get(0)+")");
			return false;
		}
		
		return proc.getExitValue() == 0;
	}

	@Override
	public String menuEntryString() {
		return "generate cube file";
	}

}
