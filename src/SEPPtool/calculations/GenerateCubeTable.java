package SEPPtool.calculations;

import java.io.IOException;
import java.io.PrintWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import SEPPtool.Calculation;
import SEPPtool.MoleculeDirectory;
import SEPPtool.SEPPtool;
import SEPPtool.cube.Cube;
import SEPPtool.cube.CubeTable;

/**
 * Generate a table of volume, area, mean radius of curvature, etc. based on
 * given values for electron density.
 * @author sebastian
 *
 */
public class GenerateCubeTable implements Calculation {
	
	final double a0 = 0.52917721067;
	
	@Override
	public boolean calculationAlreadyPerformed(MoleculeDirectory moldir) {
		return moldir.file_cubetable.isFile(); // TODO: check for valid content
	}

	@Override
	public boolean calculationReadyToPerform(MoleculeDirectory moldir) {
		return moldir.file_cubefile.isFile();
	}

	@Override
	public boolean perform(MoleculeDirectory moldir)
	{	
		Cube cube = new Cube();
		try
		{
			if (!cube.read(moldir.file_cubefile))
			{
				SEPPtool.log(moldir, "Error reading cubefile "+moldir.file_cubefile.getAbsolutePath());
				return false;
			}
		} catch (Exception e) {
			SEPPtool.log(moldir, "Error reading cubefile "+moldir.file_cubefile.getAbsolutePath()+ ": " + e.getLocalizedMessage());
			e.printStackTrace();
			return false;
		}
		
	    double numberOfElectronsTotal = cube.numberOfElectrons(Double.NEGATIVE_INFINITY);
	    
	    CubeTable cubeTable = new CubeTable(numberOfElectronsTotal);
	    
	    try {
	    	PrintWriter writer;
	    	if (SEPPtool.LEGACY)
			{ 
				writer = new PrintWriter(moldir.file_cubetablelegacy, "UTF-8");
				writer.printf("Cube %s:\n", 
			            moldir.file_cubefile.getAbsolutePath());
				writer.printf(" %12s %10s %10s %10s %10s %10s %10s %10s %10s %10s\n",
						"rho0",
						"Area", "Volume", "Rc",
						"Area", "Volume", "Rc",
						"alpha", "EP", "#electrons");
				writer.printf(" %12s %10s %10s %10s %10s %10s %10s %10s %10s %10s\n",
						"e/au³",
						"au²", "au³", "au",
						"AA²", "AA³", "AA",
						"-", "-", "e");
				writer.flush();
			}
			System.out.printf("Cube %s:\n", 
		            moldir.file_cubefile.getAbsolutePath());
			System.out.printf("%8s %12s %10s %10s %10s %10s %10s %10s %10s %10s %10s\n",
					"progress", "rho0",
					"Area", "Volume", "Rc",
					"Area", "Volume", "Rc",
					"alpha", "EP", "#electrons");
			System.out.printf("%8s %12s %10s %10s %10s %10s %10s %10s %10s %10s %10s\n",
					"%", "e/au³",
					"au²", "au³", "au",
					"AA²", "AA³", "AA",
					"-", "-", "e");
			
			for (double rho0 = 1e-6; rho0<=0.1; rho0 *= 1.047128548)
			//for (double rho0 = 1e-4; rho0<=0.001; rho0 *= 1.047128548*1.047128548*1.047128548*1.047128548*1.047128548*1.047128548)
		    {
				cube.constructSurface(rho0);
				
				double S = cube.surfaceArea();
				double V = cube.volume();
				double Rc = cube.meanRadiusOfCurvature();
				double alpha = Rc * S / 3.0 / V;
				
				double numberOfElectrons = cube.numberOfElectrons(rho0);
				
				cubeTable.addCubeTableEntry(S*a0*a0, V*a0*a0*a0, Rc*a0, numberOfElectrons);

				// percentage of enclosed electrons
				double ep = numberOfElectrons / numberOfElectronsTotal;
				
				if (SEPPtool.LEGACY)
				{ 
			        writer.printf(" %12.10f %10.3f %10.3f %10.4f %10.3f %10.3f %10.4f %10.5f %10.8f %10.5f\n", rho0,
			                S,       V,          Rc,
			                S*a0*a0, V*a0*a0*a0, Rc*a0,
			                alpha, ep, numberOfElectronsTotal);
			        writer.flush();
				}
		        System.out.printf("%7.1f%% %12.10f %10.3f %10.3f %10.4f %10.3f %10.3f %10.4f %10.5f %10.8f %10.5f\n",
		        		(Math.log10(rho0)+6)/5.0*100,
		        		rho0,
		                S,       V,          Rc,
		                S*a0*a0, V*a0*a0*a0, Rc*a0,
		                alpha, ep, numberOfElectronsTotal);
		    }
			
			if (SEPPtool.LEGACY) writer.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	    
	    try {
			JAXBContext jc = JAXBContext.newInstance(CubeTable.class);
	        Marshaller m = jc.createMarshaller();
	        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	        m.marshal( cubeTable, moldir.file_cubetable );
		} catch (JAXBException e) {
			System.err.println("Could not save cube table xml file: " + e.getLocalizedMessage());
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	@Override
	public String menuEntryString() {
		return "generate electron density table";
	}
}
