package SEPPtool.calculations;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Vector;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import SEPPtool.Calculation;
import SEPPtool.InteractionData;
import SEPPtool.MoleculeDirectory;
import SEPPtool.ResultsNonAssoc;
import SEPPtool.SEPPtool;
import SEPPtool.cube.CubeTable;
import SEPPtool.cube.Point;

/**
 * Calculate multipoles and C6 coefficient.
 * Adapted from C++ code written at the Institute for Technical Thermodynamics (LTT), RWTH Aachen University.
 * @author sebastian
 *
 */
public class Interactions implements Calculation {

	@Override
	public boolean calculationAlreadyPerformed(MoleculeDirectory moldir) {
		return moldir.file_results.isFile(); // TODO: check for valid content
	}

	@Override
	public boolean calculationReadyToPerform(MoleculeDirectory moldir) {
		Calculation c1 = new Dynpol();
		Calculation c2 = new Polar();
		return c1.calculationAlreadyPerformed(moldir)
				&& c2.calculationAlreadyPerformed(moldir);
	}

	@Override
	public boolean perform(MoleculeDirectory moldir) {
		
		InteractionData data;
		try
		{
			data = calcMultipoles(moldir);
			if (data==null) return false;
		}
		catch (FileNotFoundException e)
		{
			SEPPtool.log(moldir, "Could not calculate multipoles: " + e.getLocalizedMessage());
			e.printStackTrace();
			return false;
		}
		
		try
		{
			boolean success = calcC6(moldir,data);
			if (!success) return false;
		}
		catch (FileNotFoundException e) 
		{
			SEPPtool.log(moldir, "Could not calculate C6: " + e.getLocalizedMessage());
			e.printStackTrace();
			return false;
		}
		
		// This file exists for historic reasons and might get removed in future
		if (SEPPtool.LEGACY)
		{
			PrintWriter writer;
			try {
				writer = new PrintWriter(new FileWriter(moldir.file_results));
				writer.println(moldir.getName());
				writer.println("mu "+data.getMu());
				writer.println("teta "+data.getQ());
				writer.println("c6 "+data.getC6());
				writer.println("ip "+data.getAlpha());
				writer.close();
			} catch (IOException e) {
				SEPPtool.log(moldir, "Could not write to results file "+moldir.file_results.getAbsolutePath());
				e.printStackTrace();
			}
		}
		
		try {
			JAXBContext jc = JAXBContext.newInstance(InteractionData.class);
	        Marshaller m = jc.createMarshaller();
	        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	        m.marshal( data, moldir.file_interactiondata );
		} catch (JAXBException e) {
			System.err.println("Could not save interaction data xml file: " + e.getLocalizedMessage());
			e.printStackTrace();
			return false;
		}
		
		// put cubetable and interaction data in the same file
		
		// read cube table
		CubeTable cubeTable = null;
		try
		{
			JAXBContext jaxbContext = JAXBContext.newInstance(CubeTable.class);
		    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		    cubeTable = (CubeTable) jaxbUnmarshaller.unmarshal(moldir.file_cubetable);
		}
	    catch (JAXBException e) 
		{
			System.err.print("Could not load cube file for "+moldir.getName()+": ");
			System.err.println(e.getLocalizedMessage());
		}
		
		if (cubeTable != null)
		{
			ResultsNonAssoc rna = new ResultsNonAssoc(data, cubeTable);
			try {
				JAXBContext jc = JAXBContext.newInstance(ResultsNonAssoc.class);
		        Marshaller m = jc.createMarshaller();
		        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		        m.marshal( rna, moldir.file_resultsnonassoc );
			} catch (JAXBException e) {			
				System.err.println("Could not save results xml file: " + e.getLocalizedMessage());
				e.printStackTrace();	
			}
		}
		
		return true;
	}
	
	/**
	 * This method is based on C++ code written at the Institute for Technical Thermodynamics (LTT), RWTH Aachen University.
	 * 
	 * @param moldir
	 * @return
	 * @throws FileNotFoundException
	 */
	public InteractionData calcMultipoles(MoleculeDirectory moldir) throws FileNotFoundException
	{
		InteractionData data = new InteractionData();
		
		Scanner scanner = new Scanner(moldir.file_gauss_polar_log);
		
		if (!jumpTo(scanner,"Standard orientation:"))
		{
			scanner.close();
			return null;
		}
		
		// skip 4 lines
		for (int i=0; i<4; i++) scanner.nextLine();
	
		Vector<Atom> atoms = new Vector<Atom>();
		try
		{
			for (int i=0; i<1000; i++) // to avoid infinite loop, only scan for 1000 atoms
			{
				scanner.nextInt(); // ignore "Center Number"
				int atomicNumber = scanner.nextInt(); // "Atomic Number"
				scanner.nextInt(); // ignore "Atomic Type"
				
				Atom atom = new Atom(scanner); // reads position (x,y,z) from scanner
				atom.setAtomicNumber(atomicNumber);
				atoms.add(atom);
				System.out.printf("atom %d %d %f %f %f\n",i,atomicNumber,atom.getX(),atom.getY(),atom.getZ());
			}
		}
		catch (InputMismatchException e)
		{
			// done reading atom positions
		}
		
		
		if (!jumpTo(scanner,"Population analysis"))
		{
			SEPPtool.log(moldir, "Could not read population analysis in gaussian log file "+moldir.file_gauss_polar_log.getAbsolutePath());
			scanner.close();
			return null;
		}
		
		if (!jumpTo(scanner,"Dipole moment"))
		{
			SEPPtool.log(moldir, "Could not read dipole moment in gaussian log file "+moldir.file_gauss_polar_log.getAbsolutePath());
			scanner.close();
			return null;
		}
		
		scanner.next(); // skip x-label
		double dipole_x = scanner.nextDouble();
		scanner.next(); // skip y-label
		double dipole_y = scanner.nextDouble();
		scanner.next(); // skip z-label
		double dipole_z = scanner.nextDouble();
		Point dipole = new Point(dipole_x, dipole_y, dipole_z);
		
		if (!jumpTo(scanner,"Traceless Quadrupole moment"))
		{
			SEPPtool.log(moldir, "Could not read traceless quadrupole moment in gaussian log file "+moldir.file_gauss_polar_log.getAbsolutePath());
			scanner.close();
			return null;
		}
		
		double Q[][] = new double[3][3];
		scanner.next(); // skip XX-label
		Q[0][0] =           scanner.nextDouble();
		scanner.next(); // skip YY-label
		Q[1][1] =           scanner.nextDouble();
		scanner.next(); // skip ZZ-label
		Q[2][2] =           scanner.nextDouble();
		scanner.next(); // skip XY-label
		Q[0][1] = Q[1][0] = scanner.nextDouble();
		scanner.next(); // skip XZ-label
		Q[0][2] = Q[2][0] = scanner.nextDouble();
		scanner.next(); // skip YZ-label
		Q[1][2] = Q[2][1] = scanner.nextDouble();
		
		System.out.println("Q");
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				System.out.printf("%10.5g ",Q[i][j]);	
			}
			System.out.println();
		}
		
		String line=null;
		while (scanner.hasNextLine())
		{
			if ((line=scanner.nextLine()).contains("Exact polarizability:")) break;
		}
		if (!scanner.hasNextLine())
		{
			// TODO issue warning?
			scanner.close();
			return null;
		}
		
		Scanner lineScanner = new Scanner(line.split(":")[1]);
		double alpha[][] = new double[3][3];
		alpha[0][0] =               lineScanner.nextDouble();
		alpha[0][1] = alpha[1][0] = lineScanner.nextDouble();
		alpha[1][1] =               lineScanner.nextDouble();
		alpha[0][2] = alpha[2][0] = lineScanner.nextDouble();
		alpha[1][2] = alpha[2][1] = lineScanner.nextDouble();
		alpha[2][2] =               lineScanner.nextDouble();
		lineScanner.close();
		
		System.out.println("alpha");
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				System.out.printf("%10.5g ",alpha[i][j]);	
			}
			System.out.println();
		}
		
		scanner.close();
		// done reading log-file
		
		// multiply Q with 1.5
		for (int i = 0; i < 3; i++) for (int j = 0; j < 3; j++) Q[i][j] *= 1.5;
		
		// calculate trace of Q
		double trace = 0.0;
		for (int i = 0; i < 3; i++) trace += Q[i][i];
		System.out.printf("trace = %10.5g\n",trace);
		
		// check for tracelessness
		if (Math.abs(trace) > 1e-6)
		{
			// make Q traceless
			Q[2][2] = - Q[0][0] - Q[1][1]; 
			SEPPtool.log(moldir, "Setting Q_ZZ to "+Q[2][2]+ " to make Q traceless");
		}
		
		Point centerOfNuclearCharge = calcCenterOfNuclearCharge(atoms);
		System.out.print("CoNC: ");
		centerOfNuclearCharge.print();
		
		Point centerOfMass = calcCenterOfMass(atoms);
		System.out.print("CoM:  ");
		centerOfMass.print();
		
		Point shiftToCoM = new Point();
		for (int i = 0; i < 3; i++) shiftToCoM.set(i, centerOfMass.get(i) - centerOfNuclearCharge.get(i) );
		
		System.out.print("shift:");
		shiftToCoM.print();
		
		double[][] dummy1 = dyadicProduct(shiftToCoM, dipole);
		double[][] dummy2 = dyadicProduct(dipole, shiftToCoM);
		
		double muDotA = 0.0; // dot product of dipole and shift
		for (int i = 0; i < 3; i++) muDotA += dipole.get(i) * shiftToCoM.get(i);
		
		double[][] Q_CoM = new double[3][3];
		for (int i = 0; i < 3; i++)	for (int j = 0; j < 3; j++)
		{
			double dummy = 1.5 * ( dummy1[i][j] + dummy2[i][j] );
			if (i==j) dummy -= muDotA; // minus mu.A * Identity-Matrix
			
			Q_CoM[i][j] = Q[i][j] - dummy;
		}
		
		System.out.println("Q_CoM");
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				System.out.printf("%10.6g ",Q_CoM[i][j]);	
			}
			System.out.println();
		}
		
		Point Q_eigenvalues = eigenvalues(Q_CoM);
		System.out.print("CoM Q eigenvalues: ");
		Q_eigenvalues.print();
		
		double s_dipole = dipole.distanceTo(Point.zero);
		double s_quadrupole = Q_eigenvalues.distanceTo(Point.zero) / Math.sqrt(1.5);
		double s_polarizability = (alpha[0][0] + alpha[1][1] + alpha[2][2]) / 3.0 * 0.1482;
		
		System.out.printf("mu=%10.6g Q_CoM=%10.6g alpha=%10.6g\n", s_dipole, s_quadrupole, s_polarizability);
		
		data.setMu(s_dipole);
		data.setQ(s_quadrupole);
		data.setAlpha(s_polarizability);
		
		return data;
	}
	
	private boolean jumpTo(Scanner scanner, String text)
	{
		while (scanner.hasNextLine()) if (scanner.nextLine().contains(text)) return true;
		return false;
	}
	
	private Point calcCenterOfMass(Vector<Atom> atoms)
	{
		Point CoM = new Point(0,0,0);
		double totalMass = 0.0;
		for (Atom atom : atoms)
		{
			double atomMass = atom.getMass();
			totalMass += atomMass;
			for (int i = 0; i < 3; i++)	CoM.set(i, CoM.get(i) + atom.get(i) * atomMass );
		}
		for (int i = 0; i < 3; i++) CoM.set(i, CoM.get(i) / totalMass );
		return CoM;
	}
	
	private Point calcCenterOfNuclearCharge(Vector<Atom> atoms)
	{
		Point CoNC = new Point(0,0,0);
		double totalCharge = 0.0;
		for (Atom atom : atoms)
		{
			double atomCharge = (double)atom.getAtomicNumber();
			totalCharge += atomCharge;
			for (int i = 0; i < 3; i++)	CoNC.set(i, CoNC.get(i) + atom.get(i) * atomCharge );
		}
		for (int i = 0; i < 3; i++) CoNC.set(i, CoNC.get(i) / totalCharge );
		return CoNC;
	}
	
	private double[][] dyadicProduct(Point p1, Point p2)
	{
		double[][] dyad = new double[3][3];
		for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
		{
			dyad[i][j] = p1.get(i) * p2.get(j);
		}
		return dyad;
	}
	
	private Point eigenvalues(double[][] A)
	{
		if (A.length != 3) return null; // not 3 rows
		for (int i = 0; i < 3; i++)
		{
			if (A[i].length != 3) return null; // not 3 columns in each row
			
			for (int j = i+1; j < 3; j++)
				if (A[i][j] != A[j][i]) // not symmetric
					return null;
		}
		
		Point eig = new Point();
		
		// algorithm:
		// Smith, Oliver K. (April 1961),
		// "Eigenvalues of a symmetric 3 × 3 matrix.",
		// Communications of the ACM, 4 (4): 168
		// https://doi.org/10.1145/355578.366316
		
		// Given a real symmetric 3x3 matrix A, compute the eigenvalues
		// Note that acos and cos operate on angles in radians

		//p1 = A(1,2)^2 + A(1,3)^2 + A(2,3)^2
		double p1 = A[0][1] * A[0][1] + A[0][2]*A[0][2] + A[1][2]*A[1][2];
				
		if (p1 == 0.0)
		{
			// A is diagonal.
			//eig1 = A(1,1)
			//eig2 = A(2,2)
			//eig3 = A(3,3)
			for (int i = 0; i < 3; i++)
				eig.set(i, A[i][i]);
		}
		else
		{
			//q = trace(A)/3               % trace(A) is the sum of all diagonal values
			double q = (A[0][0] + A[1][1] + A[2][2] ) / 3.0;
			//p2 = (A(1,1) - q)^2 + (A(2,2) - q)^2 + (A(3,3) - q)^2 + 2 * p1
			double p2 = Math.pow(A[0][0] - q, 2) + Math.pow(A[1][1] - q, 2) + Math.pow(A[2][2] - q, 2) + 2 * p1; 
			//p = sqrt(p2 / 6)
			double p = Math.sqrt(p2 / 6.0);
			// B = (1 / p) * (A - q * I)    % I is the identity matrix
			double[][] B = new double[3][3];
			for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
			{
				B[i][j] = A[i][j] / p;
				if (i==j) B[i][j] -= q / p;
			}
			//r = det(B) / 2
			double r = det(B) / 2;

			//% In exact arithmetic for a symmetric matrix  -1 <= r <= 1
			//% but computation error can leave it slightly outside this range.
			double phi;
			if (r <= -1.0)
				phi = Math.PI / 3.0;
			else if (r >= 1.0)
				phi = 0.0;
			else
				phi = Math.acos(r) / 3.0;

			// the eigenvalues satisfy eig3 <= eig2 <= eig1
			//eig1 = q + 2 * p * cos(phi)
			//eig3 = q + 2 * p * cos(phi + (2*pi/3))
			//eig2 = 3 * q - eig1 - eig3     % since trace(A) = eig1 + eig2 + eig3
			
			eig.set(0, q + 2.0 * p * Math.cos(phi) );
			eig.set(2, q + 2.0 * p * Math.cos(phi + Math.PI / 1.5) );
			eig.set(1, 3.0 * q - eig.get(0) - eig.get(2) );
		}
		
		return eig;
	}
	
	private double det(double[][] matrix)
	{
		if (matrix.length != 3) return Double.NaN;
		for (int i = 0; i < 3; i++) if (matrix[i].length != 3) return Double.NaN;
		
		return    matrix[0][0] * (matrix[1][1] * matrix[2][2] - matrix[1][2] * matrix[2][1])
				+ matrix[0][1] * (matrix[1][2] * matrix[2][0] - matrix[1][0] * matrix[2][2])
				+ matrix[0][2] * (matrix[1][0] * matrix[2][1] - matrix[1][1] * matrix[2][0]);
	}
	
	/**
	 * This method is based on C++ code written at the Institute for Technical Thermodynamics (LTT), RWTH Aachen University.
	 * 
	 * @param moldir
	 * @param data
	 * @return
	 * @throws FileNotFoundException
	 */
	public boolean calcC6(MoleculeDirectory moldir, InteractionData data) throws FileNotFoundException
	{
		if (!moldir.file_turbomoledir.isDirectory())
		{
			SEPPtool.log(moldir, "Turbomole directory "+moldir.file_turbomoledir.getAbsolutePath()+" does not exists");
			return false;
		}
		
		// create list of frequencies and list of polarizabilities
		double[] frequencies = new double[Dynpol.frequencies.length+1];
		double[] polarizabilities = new double[frequencies.length];
		frequencies[0] = 0.0;
		polarizabilities[0] = -1;
		for (int i = 1; i < frequencies.length; i++)
		{
			frequencies[i] = Dynpol.frequencies[i-1];
			polarizabilities[i] = -1;
		}
		
		for (File file : moldir.file_turbomoledir.listFiles(new FileFilter(){
			@Override
			public boolean accept(File pathname) {
				return pathname.getName().endsWith("_escf.out");
			}}))
		{
			
			System.out.println("File "+file.getAbsolutePath());
			Scanner scanner = new Scanner(file);
			
			while (true)
			{
				String line = null;
				while (scanner.hasNextLine())
				{
					if ((line=scanner.nextLine()).contains("Frequency:")) break;
				}
				if (!scanner.hasNextLine()) break;
				
				Scanner lineScanner = new Scanner(line.split("Frequency:")[1]); // scan line after first appearance of "Frequency:"
				double freq = lineScanner.nextDouble();
				lineScanner.close();
				
				while (scanner.hasNextLine())
				{
					if ((line=scanner.nextLine()).contains("1/3*trace:")) break;
				}
				if (!scanner.hasNextLine()) break;
				lineScanner = new Scanner(line.split("trace:")[1]); // scan line after first appearance of "trace:"
				double pol = lineScanner.nextDouble();
				lineScanner.close();
				
				// find index of frequency
				boolean found=false;
				for (int i = 0; i < frequencies.length; i++)
				{
					if (Math.abs(frequencies[i] - freq) < 1e-6)
					{
						if (polarizabilities[i]>=0)
						{
							SEPPtool.log(moldir, "frequency "+freq+" was read twice, old value: "+polarizabilities[i]+", new value:"+pol);
						}
						found=true;
						polarizabilities[i] = pol;
						break;
					}
				}
				if (!found)
				{
					SEPPtool.log(moldir, "warning: frequency " + freq + " was calculated, but is not needed.");
				}
				
				System.out.printf("%10.6g %10.6g\n", freq, pol);
			}
			scanner.close();
		}
		// done reading frequencies from files
		
		double[] alpha_scaled = new double[frequencies.length];
		double q = data.getAlpha() / 0.1482 / polarizabilities[0];
		System.out.printf("alpha %10.6g %10.6g q = %10.6g\n", data.getAlpha() / 0.1482, polarizabilities[0], q);
		for (int i = 0; i < frequencies.length; i++)
		{
			if (polarizabilities[i] < 0)
			{
				SEPPtool.log(moldir, "No polarizability for frequency " + frequencies[i] + " was read.");
				return false;
			}
			
			double omega = frequencies[i];
			if (q>1)
			    alpha_scaled[i] = polarizabilities[i] / (1 - Math.exp( Math.log((q-1)/q) - Math.pow(omega, 0.7) ));
			else if (q<1)
			    alpha_scaled[i] = polarizabilities[i] * (1 - Math.exp( Math.log(1-q) - Math.pow(omega, 0.7) ));
			
			System.out.printf("freqency = %10.6g polarizability = %10.6g scaled polarizability =  %10.6g\n",
					frequencies[i], polarizabilities[i], alpha_scaled[i]);
		}
		
		  ///////////////////////////////////
		 // Gauss-Legendre transformation //
		///////////////////////////////////
		
		double LowerLim = 0;
		double UpperLim = 20;
	    //double xm = 0.5 * (UpperLim + LowerLim);
	    double xl = 0.5 * (UpperLim - LowerLim);
		
		int NoOfPoints = frequencies.length - 1; // subtract 1 for frequency=0
		//double[] x = new double[NoOfPoints];
		double[] w = new double[NoOfPoints];
		double sum = 0.0;
	    for (int i = 0; i < NoOfPoints; i++) 
	    {			                        
			// Starting with the above approximation to the ith root, 
			// we enter the main loop of refinement by Newton's method
			double z = Math.cos( Math.PI * (i + 0.75) / (NoOfPoints + 0.5) );
			double z1;
			double eps = 1e-14;
			
			double pp;
			do 
			{
			    double p1 = 1.0;
			    double p2 = 0.0;
			    
			    // Loop up the recurrence relation to get the 
			    // Legendre polynomial evaluated at z
			    for (int j = 0; j < NoOfPoints; j++) 
			    {    	
					double p3 = p2;
					p2 = p1;
					p1 = ( (2.0 * j + 1.0) * z * p2 - j * p3 ) / (j + 1);
			    }
			    
			    // p1 is now the desired Legendre polynomial. We 
			    // next compute pp, its derivative, by a standard 
			    // relation involving also p2, the polynomial of 
			    // one lower order
			    pp = NoOfPoints * (z * p1 - p2) / (z * z - 1.0);
			    z1 = z;
			    
			    // Newton's Method
			    z = z1 - p1 / pp;           	
			    
			} while (Math.abs(z - z1) > eps);
			
			// Scale the root to the desired interval
			//x[i] = xm - xl * z;
			
			// put in its symmetric counterpart
			//x[NoOfPoints - 1 - i] = xm + xl * z;
			
			// Compute the weight
			w[i] = 2.0*xl/((1.0-z*z)*pp*pp);
			
			// its symmetric counterpart
			//w[NoOfPoints - 1 - i] = w[i];
			
			sum += w[i] * alpha_scaled[i + 1] * alpha_scaled[i + 1];
			
	    }
		System.out.printf("C6 = %10.6f\n",3 * sum / Math.PI);
	    data.setC6( 3 * sum / Math.PI );
	    
		return true;
	}

	@Override
	public String menuEntryString() {
		return "Calculate Multipoles and C6";
	}

}


class Atom extends Point
{
	int atomicNumber;
	
	public Atom(Scanner scanner)
	{
		super(scanner);
	}

	public int getAtomicNumber() { return atomicNumber; }
	public void setAtomicNumber(int atomicNumber) { this.atomicNumber = atomicNumber; }
	
	public double getMass()
	{
		switch(atomicNumber)
		{
		case 1:
			return 1.00797;
		case 6:
			return 12.011;
		case 7:
			return 14.0067;
		case 8:
		    return 15.9994;
		case 9:
		    return 18.998403;
		case 16:
		    return 32.06;
		case 17:
		    return 35.453;
		default:
		    System.out.printf("Atomic mass for element with atomic number %d is not available!\n",
		 		   atomicNumber);
			return Double.NaN;
		}
	}
}
