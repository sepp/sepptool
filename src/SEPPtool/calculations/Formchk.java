package SEPPtool.calculations;

import java.io.IOException;
import java.util.ArrayList;

import SEPPtool.Calculation;
import SEPPtool.MoleculeDirectory;
import SEPPtool.ProcessRunner;
import SEPPtool.SEPPtool;

/**
 * Format checkpoint file with gaussian's formchk
 * @author sebastian
 *
 */
public class Formchk implements Calculation {

	@Override
	public boolean calculationAlreadyPerformed(MoleculeDirectory moldir) {
		return moldir.file_fchk.isFile(); // TODO check content
	}

	@Override
	public boolean calculationReadyToPerform(MoleculeDirectory moldir) {
		return moldir.file_chk.isFile(); // TODO check content
	}

	@Override
	public boolean perform(MoleculeDirectory moldir) {
		
		ArrayList<String> alist = new ArrayList<String>();

		// add the list of commands to the list
		alist.add(SEPPtool.config.getPathFormchk());
		alist.add(moldir.file_chk.getAbsolutePath());
		alist.add(moldir.file_fchk.getAbsolutePath());
		
		ProcessRunner proc = new ProcessRunner(alist);
		proc.putEnvironmentVariable("GAUSS_EXEDIR", SEPPtool.config.getPathGaussExedir()); 
		proc.putEnvironmentVariable("GAUSS_SCRDIR", SEPPtool.config.getPathGaussScrdir());
		try {
			proc.run();
		} catch (IOException|InterruptedException e) {
			SEPPtool.log(moldir, "Error calling formchk ("+alist.get(0)+")");
			return false;
		}
		
		return proc.getExitValue() == 0;
	}

	@Override
	public String menuEntryString() {
		return "format CHK-file";
	}

}
