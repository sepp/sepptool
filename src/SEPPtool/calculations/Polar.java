package SEPPtool.calculations;

import java.io.IOException;
import java.util.ArrayList;

import SEPPtool.Calculation;
import SEPPtool.MoleculeDirectory;
import SEPPtool.ProcessRunner;
import SEPPtool.SEPPtool;

/**
 * Calculate multipoles and static polarizability with gaussian
 * @author sebastian
 *
 */
public class Polar implements Calculation {
	
	Calculation geoOpt = new GeometryOptimization();

	@Override
	public boolean calculationAlreadyPerformed(MoleculeDirectory moldir) {
		return moldir.file_gauss_polar_log.isFile() && moldir.file_chk.isFile(); // TODO: check for valid content
	}

	@Override
	public boolean calculationReadyToPerform(MoleculeDirectory moldir) {
		return geoOpt.calculationAlreadyPerformed(moldir) || moldir.file_gauss_geo_com.isFile() || moldir.file_cosmo.isFile();
	}

	@Override
	public boolean perform(MoleculeDirectory moldir) {
		
		if (!moldir.file_gauss_polar_com.isFile())
		{
			if (geoOpt.calculationAlreadyPerformed(moldir))
			{
				// create input from optimized geometry
				if (!createInputFromOptimizedGeometry(moldir)) return false; // abort, if creating input failed
			}
			else // geometry optimization not performed
			{
				if (!(moldir.file_gauss_geo_com.isFile() || moldir.file_cosmo.isFile()))
				{
					SEPPtool.log(moldir, "A geometry must be given!");
					return false;
				}
				else
				{
					SEPPtool.log(moldir, "Geometry not optimized, using starting geometry!");
					if (!createInputFromStartingGeometry(moldir)) return false; // abort, if creating input failed
				}
			}
		}
		
		if (!moldir.file_cubedir.exists())
		{
			if (!moldir.file_cubedir.mkdirs())
			{
				SEPPtool.log(moldir, "Could not create directory "+moldir.file_cubedir.getAbsolutePath());
				return false;
			}
		}
		else if (!moldir.file_cubedir.isDirectory()) // cubedir exists; check if it is a directory
		{
			SEPPtool.log(moldir, "Error: "+moldir.file_cubedir.getAbsolutePath()+" already exists, but is not a directory.");
			return false;
		}
		
		ArrayList<String> alist = new ArrayList<String>();

		// add the list of commands to the list
		alist.add(SEPPtool.config.getPathGaussian());
		alist.add(moldir.file_gauss_polar_com.getAbsolutePath());
		
		ProcessRunner proc = new ProcessRunner(alist);
		proc.putEnvironmentVariable("GAUSS_EXEDIR", SEPPtool.config.getPathGaussExedir()); 
		proc.putEnvironmentVariable("GAUSS_SCRDIR", SEPPtool.config.getPathGaussScrdir());
		try {
			proc.run();
		} catch (IOException|InterruptedException e) {
			SEPPtool.log(moldir, "Error calling gaussian for geometry optimization ("+alist.get(0)+")");
			return false;
		}
		
		return proc.getExitValue() == 0;
	}
	
	private boolean createInputFromOptimizedGeometry(MoleculeDirectory moldir)
	{
		ArrayList<String> alist = new ArrayList<String>();

		// add the list of commands to the list
		alist.add(SEPPtool.config.getPathObabel());
		alist.add(moldir.file_gauss_geo_log.getAbsolutePath());
		alist.add("--title");
		alist.add(moldir.getName() +" geometry optimizazion");
		alist.add("-xb"); // create bond infos
		alist.add("-xk"); // include keywords
		alist.add("%chk=" + moldir.file_chk.getAbsolutePath() + "\n"+
				"%mem=30GB\n" + 
				"# MP2/aug-cc-pVDZ geom=connectivity density polar MaxDisk=200000000000");
		alist.add("-o");
		alist.add("com");
		alist.add("-O");
		alist.add(moldir.file_gauss_polar_com.getAbsolutePath());
		
		ProcessRunner proc = new ProcessRunner(alist);
		try {
			proc.run();
			if (proc.getExitValue() == 0 && proc.getErr().get(0).contains("1 molecule converted"))
			{
				SEPPtool.log(moldir, "Input file for polar calculation with gaussian generated");
				return true;
			}
			else
			{
				SEPPtool.log(moldir, "Error writing gaussian input file for polar calculations:");
				SEPPtool.log(moldir, "---Error Stream---");
				for (String s:proc.getErr()) SEPPtool.log(moldir, s);
				SEPPtool.log(moldir, "---Output Stream---");
				for (String s:proc.getOut()) SEPPtool.log(moldir, s);
				SEPPtool.log(moldir, "---End of output---");
				return false;
			}
		} catch (IOException|InterruptedException e) {
			SEPPtool.log(moldir, "Error calling obabel (" + alist.get(0) + "): " + e.getLocalizedMessage());
			return false;
		}
	}
	
	private boolean createInputFromStartingGeometry(MoleculeDirectory moldir)
	{
		System.out.println("Creating input from starting geometry not implemented yet");
		// TODO:stub
		return false;
	}

	@Override
	public String menuEntryString() {
		return "perform polar calculations with MP2/aug-cc-pVDZ";
	}
}
