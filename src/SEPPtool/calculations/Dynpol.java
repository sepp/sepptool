package SEPPtool.calculations;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;

import SEPPtool.Calculation;
import SEPPtool.MoleculeDirectory;
import SEPPtool.ProcessRunner;
import SEPPtool.SEPPtool;

/**
 * Calculate dynamic polarizabilities with turbomole
 * @author sebastian
 *
 */
public class Dynpol implements Calculation {
	
	// calculate a chunk of "freqChunkSize" during one turbomole call
	final int freqChunkSize = 4;
	final int nFreq=32;
	
	Calculation geoOpt = new GeometryOptimization();
	
	public static double[] frequencies = new double[] {
			0.0273614,
			0.143885,
			0.352377,
			0.650939,
			1.03679,
			1.50632,
			2.05516,
			2.67818,
			3.36956,
			4.12284,
			4.931,
			5.78649,
			6.68131,
			7.60713,
			8.55528,
			9.51692,
			10.4831,
			11.4447,
			12.3929,
			13.3187,
			14.2135,
			15.069,
			15.8772,
			16.6304,
			17.3218,
			17.9448,
			18.4937,
			18.9632,
			19.3491,
			19.6476,
			19.8561,
			19.9726
	};
	
	String dynpol_pre =
			"\n" +
			"dynamic polarizability for c6\n" +
			"a coord\n" +
			"*\n" +
			"no\n" +
			"b\n" +
			"all aug-cc-pVDZ\n" +
			"*\n" +
			"eht\n" +
			"\n" +
			"\n" + 
			"\n" +
			"ex\n" +
			"dynpol\n" +
			"*\n" +
			"au\n" +
			"a";
	
	String dynpol_post =
			"*\n" +
			"*\n" +
			"rpacor 300\n" +
			"*\n" +
			"\n" +
			"*\n" +
			"";
	
	String[] polly_input = new String[] {
		"",
		"static polarizability for c6",
		"a coord",
		"*",
		"no",
		"b",
		"all aug-cc-pVDZ",
		"*",
		"eht",
		"",
		"",
		"",
		"ex",
		"polly",
		"*",
		"*",
		"",
		"*",
		"",
		"*",
		""
	};


	@Override
	public boolean calculationAlreadyPerformed(MoleculeDirectory moldir) {
		// TODO more detailed check for successfull calculation
		return moldir.file_turbomoledir.isDirectory();
	}

	@Override
	public boolean calculationReadyToPerform(MoleculeDirectory moldir) {
		return geoOpt.calculationAlreadyPerformed(moldir) || moldir.file_gauss_geo_com.isFile() || moldir.file_cosmo.isFile();
	}

	@Override
	public boolean perform(MoleculeDirectory moldir) {
		
		// create directory for conformation
		if (!moldir.file_turbomoledir.exists())
		{
			if (!moldir.file_turbomoledir.mkdirs())
			{
				SEPPtool.log(moldir, "Could not create directory "+moldir.file_turbomoledir.getAbsolutePath());
				return false;
			}
		}
		else
		{
			if (!moldir.file_turbomoledir.isDirectory())
			{
				SEPPtool.log(moldir, "File "+moldir.file_turbomoledir.getAbsolutePath()+" already exists, but is no directory");
				return false;
			}
		}
		
		ArrayList<String> alist = new ArrayList<String>();

		// add the list of commands to the list
		alist.add(SEPPtool.config.getPathTurbomoleX2T());
		alist.add(moldir.file_gauss_polar_com.getAbsolutePath());
		ProcessRunner proc = new ProcessRunner(alist);
		try {
			proc.run(new File(moldir.file_turbomoledir.getAbsolutePath()+"/coord"));
		} catch (IOException|InterruptedException e) {
			SEPPtool.log(moldir, "Error calling turbomole ("+alist.get(0)+"): "+e.getLocalizedMessage());
			return false;
		}
		
		if (proc.getExitValue() != 0)
		{
			SEPPtool.log(moldir, "Could not create turbomole coord file.");
			return false;
		}
		
		for (int i = -1; i < (nFreq-1)/freqChunkSize+1; i++) // i=-1 means calling polly, i>=0 means calling dynpol
		{
			File folder;
			if (i<0)
				folder = new File(moldir.file_turbomoledir.getAbsolutePath()+"/polly");
			else
				folder = new File(moldir.file_turbomoledir.getAbsolutePath()+"/dynpol_"+i);
			
			if (!folder.exists())
			{
				if (!folder.mkdir())
				{
					SEPPtool.log(moldir, "Could not create directory "+folder.getAbsolutePath());
					return false;
				}
			}
			else
			{
				if (!folder.isDirectory())
				{
					SEPPtool.log(moldir, "File "+folder.getAbsolutePath()+" already exists, but is no directory");
					return false;
				}
			}
			
			try {
				Files.copy(Paths.get(moldir.file_turbomoledir.getAbsolutePath(), "coord"),
						Paths.get(folder.getAbsolutePath(), "coord"),
						StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e1) {
				SEPPtool.log(moldir, "Error copying turbomole coord file to subdirectory "+folder.getAbsolutePath()+": "+e1.getLocalizedMessage());
				e1.printStackTrace();
				return false;
			}
			
			ArrayList<String> defineInput = new ArrayList<String>();
			if (i<0)
			{
				defineInput.addAll(Arrays.asList(polly_input));
			}
			else
			{
				defineInput.add(dynpol_pre);
				for (int j = 0; j < freqChunkSize && i*freqChunkSize+j < nFreq; j++)
				{
					defineInput.add(frequencies[ i * freqChunkSize + j ]+ " i");
				}
				defineInput.add(dynpol_post);
			}
			
			File fileDefineOut;
			File fileDSCFOut;
			File fileESCFOut;
			
			if (i<0)
			{
				fileDefineOut = new File(moldir.file_turbomoledir.getAbsolutePath()+"/polly_define.out");
				fileDSCFOut   = new File(moldir.file_turbomoledir.getAbsolutePath()+"/polly_dscf.out");
				fileESCFOut   = new File(moldir.file_turbomoledir.getAbsolutePath()+"/polly_escf.out");
			}
			else
			{
				fileDefineOut = new File(moldir.file_turbomoledir.getAbsolutePath()+"/dynpol_"+i+"_define.out");
				fileDSCFOut = new File(moldir.file_turbomoledir.getAbsolutePath()+"/dynpol_"+i+"_dscf.out");
				fileESCFOut = new File(moldir.file_turbomoledir.getAbsolutePath()+"/dynpol_"+i+"_escf.out");
			}
			
			ProcessRunner procDefine = new ProcessRunner(SEPPtool.config.getPathTurbomoleDefine(), folder);
			try {
				procDefine.run(defineInput, fileDefineOut);
			} catch (IOException|InterruptedException e) {
				SEPPtool.log(moldir, "call of define " + i + " threw an exception: " + e.getLocalizedMessage());
				return false;
			}
			if (procDefine.getExitValue() != 0)
			{
				SEPPtool.log(moldir, "define "+i+" returned with exit value " + procDefine.getExitValue());
				return false;	
			}
			
			// set escfiterlimit to 100
			try {
				// read control file
				File controlFile = new File(folder.getAbsoluteFile()+"/control");
				FileInputStream fstream = new FileInputStream(controlFile);
				BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
				String line;
				Vector<String> lines = new Vector<String>();
				while ((line = br.readLine()) != null)   {
					lines.add(line);
				}
				br.close();
				fstream.close();
				
				// write control file
				PrintWriter pr = new PrintWriter(new BufferedWriter(new FileWriter(controlFile)));
				for (String line1 : lines)
				{
					// insert "$escfiterlimit 100" just before "$end"
					if (line1.startsWith("$end")) pr.println("$escfiterlimit 100");
					pr.println(line1);
				}
				pr.close();
				
			} catch (FileNotFoundException e1) {
				SEPPtool.log(moldir, "Could not read control file in " + folder.getAbsolutePath()+": "+ e1.getLocalizedMessage());
				return false;
			} catch (IOException e) {
				SEPPtool.log(moldir, "Could not write control file in " + folder.getAbsolutePath()+": "+e.getLocalizedMessage());
				return false;
			}
			
			ProcessRunner procDSCF = new ProcessRunner(SEPPtool.config.getPathTurbomoleDSCF(), folder);
			try {
				procDSCF.run(fileDSCFOut);
			} catch (IOException | InterruptedException e) {
				SEPPtool.log(moldir, "call of dscf " + i + " threw an exception: " + e.getLocalizedMessage());
				return false;
			}
			if (procDSCF.getExitValue() != 0)
			{
				SEPPtool.log(moldir, "dscf "+i+" returned with exit value " + procDSCF.getExitValue());
				return false;
			}
			
			ProcessRunner procESCF = new ProcessRunner(SEPPtool.config.getPathTurbomoleESCF(), folder);
			try {
				procESCF.run(fileESCFOut);
			} catch (IOException | InterruptedException e) {
				SEPPtool.log(moldir, "call of escf " + i + " threw an exception: " + e.getLocalizedMessage());
				return false;
			}
			if (procESCF.getExitValue() != 0)
			{
				SEPPtool.log(moldir, "escf "+i+" returned with exit value " + procESCF.getExitValue());
				return false;
			}
		}
		
		return true;
	}

	@Override
	public String menuEntryString() {
		return "calculate dynamic polarizabilities";
	}
}
