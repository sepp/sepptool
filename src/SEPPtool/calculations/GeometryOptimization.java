package SEPPtool.calculations;

import java.io.IOException;
import java.util.ArrayList;

import SEPPtool.Calculation;
import SEPPtool.MoleculeDirectory;
import SEPPtool.ProcessRunner;
import SEPPtool.SEPPtool;
import SEPPtool.cosmo.COSMOFile;

/**
 * Optimize geometry with gaussian
 * @author sebastian
 *
 */
public class GeometryOptimization implements Calculation {

	@Override
	public boolean calculationAlreadyPerformed(MoleculeDirectory moldir) {
		return moldir.file_gauss_geo_log.isFile(); // TODO: check for valid content
	}

	@Override
	public boolean calculationReadyToPerform(MoleculeDirectory moldir) {
		return moldir.file_gauss_geo_com.isFile() || moldir.file_cosmo.isFile(); //TOOD check validity
	}

	@Override
	public boolean perform(MoleculeDirectory moldir) {
		
		if (!moldir.file_gauss_geo_com.isFile())
		{
			if (moldir.file_cosmo.isFile())
			{
				if (!cosmoToCom(moldir))
					return false;
			}
			else
			{
				SEPPtool.log(moldir, "A geometry must be given!");
				return false;
			}
		}
		
		ArrayList<String> alist = new ArrayList<String>();

		// add the list of commands to the list
		alist.add(SEPPtool.config.getPathGaussian());
		alist.add(moldir.file_gauss_geo_com.getAbsolutePath());
		
		ProcessRunner proc = new ProcessRunner(alist);
		proc.putEnvironmentVariable("GAUSS_EXEDIR", SEPPtool.config.getPathGaussExedir()); 
		proc.putEnvironmentVariable("GAUSS_SCRDIR", SEPPtool.config.getPathGaussScrdir());
		try {
			proc.run();
		} catch (IOException|InterruptedException e) {
			SEPPtool.log(moldir, "Error calling gaussian for geometry optimization ("+alist.get(0)+")");
			return false;
		}
		
		return proc.getExitValue() == 0;
	}

	@Override
	public String menuEntryString() {
		return "optimize geometry with B3LYP/TZVP";
	}
	
	private boolean cosmoToCom(MoleculeDirectory moldir)
	{
		COSMOFile cfile;
		try {
			cfile = new COSMOFile(moldir.file_cosmo.getAbsolutePath()); // TODO change COSMOFile to accept File instead of String
		} catch (IOException e) {
			SEPPtool.log(moldir, "Could not read cosmo file " + moldir.file_cosmo.getAbsolutePath()+": "+e.getLocalizedMessage());
			e.printStackTrace();
			return false;
		}
		
		ArrayList<String> alist = new ArrayList<String>();

		// add the list of commands to the list
		alist.add(SEPPtool.config.getPathObabel());
		alist.add("--title");
		alist.add(moldir.getName() +" geometry optimizazion");
		alist.add("-xb");
		alist.add("-xk");
		alist.add("%mem=30GB\n# opt b3lyp geom=connectivity tzvp");
		alist.add("-i");
		alist.add("xyz");
		alist.add("-o");
		alist.add("com");
		alist.add("-O");
		alist.add(moldir.file_gauss_geo_com.getAbsolutePath());
		
		ArrayList<String> input = new ArrayList<String>();
		input.add("" + cfile.getAtoms().size());
		input.add(moldir.getName());
		for (COSMOFile.Atom atom : cfile.getAtoms())
			input.add(" "+atom.getElement()+" "+atom.getX()+" "+atom.getY()+" "+atom.getZ());
		
		SEPPtool.log(moldir, "Creating gaussian input file from COSMO file");
		
		ProcessRunner proc = new ProcessRunner(alist);
		try {
			proc.run(input);
		} catch (IOException|InterruptedException e) {
			SEPPtool.log(moldir, "Error calling obabel ("+alist.get(0)+")");
			return false;
		}	
		
		if (proc.getExitValue() == 0 && proc.getErr().get(0).contains("1 molecule converted"))
			SEPPtool.log(moldir, "Input file for geometry optimization with gaussian generated");
		else
		{
			SEPPtool.log(moldir, "Error converting COSMO file to gaussian input file:");
			for (String s:proc.getErr()) SEPPtool.log(moldir, s);
			return false;
		}
		
		return true;
	}
}
