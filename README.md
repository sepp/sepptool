# SEPPtool

Segment-based Equation of state Parameter Prediction (SEPP) is a method to predict parameter for SAFT-like equations of state based on quantum-mechanical (QM) calculations.
Currently SEPP is parameterized for PC-SAFT and SAFT-VR Mie with a 12-6 Lennord Jones potential.
SEPPtool provides a user interface to manage the different kinds of QM calculations needed for SEPP.
The current code is optimized and tested for the RWTH high performance compute cluster using the SLURM batch system.
If you need guidance to get the code running in your environment, please contact us.

If you are using SEPP, please cite

> Kaminski, S. & Leonhard, K.  
> [SEPP: Segment-Based Equation of State Parameter Prediction](https://doi.org/10.1021/acs.jced.0c00733)  
> _Journal of Chemical & Engineering Data_, **2020**, 65, 5830-5843  

## Prerequisites

### Required software
* Gaussian (g09, formchk, cubegen)
* Turbomole (x2t, define, dscf, escf)
* OpenBabel (obabel)

### Installation
Clone git repository:

    git clone https://git.rwth-aachen.de/sepp/sepptool.git
    cd sepptool

Get and unzip apache commons math3 library:

    wget https://ftp-stud.hs-esslingen.de/pub/Mirrors/ftp.apache.org/dist/commons/math/binaries/commons-math3-3.6.1-bin.zip
    unzip commons-math3-3.6.1-bin.zip

compile and package into jar

    mkdir bin
    javac -cp commons-math3-3.6.1/commons-math3-3.6.1.jar -d bin -sourcepath src src/SEPPtool/*.java src/SEPPtool/*/*.java 
    jar cvmf src/META-INF/MANIFEST.MF SEPPtool.jar -C bin .

run SEPPtool

    java -jar SEPPtool.jar

